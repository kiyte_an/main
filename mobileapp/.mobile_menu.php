<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mobileapp/public/.mobile_menu.php");

$arMobileMenuItems = array(
	array(
		"type" => "section",
		"text" => GetMessage("MOBILE_MENU_HEADER"),
		"sort" => "100",
		"items" => array(
			array(
				"text" => 'Содать отчет',
				"data-url" => SITE_DIR . "mobileapp/index.php",
				"class" => "menu-item",
				"id" => "main",

			),
			array(
				"text" => 'Результаты отчетов',
				"data-url" => SITE_DIR . "mobileapp/report-list/index.php",
				"class" => "menu-item",
				"id" => "point4",

			),
			array(
				"text" => 'Офлайн бланк',
				"data-url" => SITE_DIR . "mobileapp/report-offline/index.php",
				"class" => "menu-item",
				"id" => "point4",

			),
			array(
				"text" => 'Склады',
				"data-url" => SITE_DIR . "mobileapp/warehouse/index.php",
				"class" => "menu-item",
				"id" => "point4",

			)
		)
	)
);
?>