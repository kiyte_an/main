<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("report-offline");
?>
<p>Для заполнения отчета в режиме офлайн откройте бланк, введите нужные значения, сохраните файл с введеными данными.</p>
<a href="#" onclick="showFile();" >Бланк</a>
<script>
function showFile() {
  BXMobileApp.UI.Document.open({"url":"/mobileapp/offline/report.xlsx"});
}
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>