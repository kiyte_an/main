<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");?>
<p>
	FLapp - приложение работает в тестовом режиме.
</p>
<p>
	 Для создания отчета перейдите в соответсвующий пункт меню.
</p>
<?$APPLICATION->IncludeComponent("bitrix:iblock.element.add", "mobile", Array(
	"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"ALLOW_DELETE" => "Y",	// Разрешать удаление
		"ALLOW_EDIT" => "Y",	// Разрешать редактирование
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",	// * дата начала *
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",	// * дата завершения *
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",	// * подробная картинка *
		"CUSTOM_TITLE_DETAIL_TEXT" => "",	// * подробный текст *
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",	// * раздел инфоблока *
		"CUSTOM_TITLE_NAME" => "",	// * наименование *
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",	// * картинка анонса *
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",	// * текст анонса *
		"CUSTOM_TITLE_TAGS" => "",	// * теги *
		"DEFAULT_INPUT_SIZE" => "30",	// Размер полей ввода
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",	// Использовать упрощенный визуальный редактор для редактирования подробного текста
		"ELEMENT_ASSOC" => "CREATED_BY",	// Привязка к пользователю
		"GROUPS" => array(	// Группы пользователей, имеющие право на добавление/редактирование
			0 => "2",
		),
		"IBLOCK_ID" => "1",	// Инфоблок
		"IBLOCK_TYPE" => "content",	// Тип инфоблока
		"LEVEL_LAST" => "Y",	// Разрешить добавление только на последний уровень рубрикатора
		"MAX_FILE_SIZE" => "0",	// Максимальный размер загружаемых файлов, байт (0 - не ограничивать)
		"MAX_LEVELS" => "100000",	// Ограничить кол-во рубрик, в которые можно добавлять элемент
		"MAX_USER_ENTRIES" => "100000",	// Ограничить кол-во элементов для одного пользователя
		"NAV_ON_PAGE" => "10",	// Количество элементов на странице
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",	// Использовать упрощенный визуальный редактор для редактирования текста анонса
		"PROPERTY_CODES" => array(	// Свойства, выводимые на редактирование
			0 => "1",
			1 => "2",
			2 => "3",
			3 => "4",
			4 => "5",
			5 => "NAME",
		),
		"PROPERTY_CODES_REQUIRED" => "",	// Свойства, обязательные для заполнения
		"RESIZE_IMAGES" => "N",	// Использовать настройки инфоблока для обработки изображений
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"STATUS" => "ANY",	// Редактирование возможно
		"STATUS_NEW" => "N",	// Деактивировать элемент после сохранения
		"USER_MESSAGE_ADD" => "",	// Сообщение об успешном добавлении
		"USER_MESSAGE_EDIT" => "",	// Сообщение об успешном сохранении
		"USE_CAPTCHA" => "N",	// Использовать CAPTCHA
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
<script>
app.exec("getToken", {
    callback:function(token)
    {
    var platform = (window.platform == "ios"? "APPLE": "GOOGLE");
    
    var config =
    {
        url: "myurl.php" ,
        method: "POST",
        data: {
            device_name: (typeof device.name == "undefined" ? device.model : device.name),
            uuid: device.uuid,
            device_token: token,
            app_id: "BitrixMobile",
            device_type: platform,
            sessid: BX.bitrix_sessid()
        }
    };

    BX.ajax(config);
}

});
</script>
<?
if (CModule::IncludeModule("pull"))
{
    $arMessages = array();
    $message = Array(
        "USER_ID" => 1, //Идентификатор пользователя
        "TITLE" => "Title", //заголовок, только для Android
        "APP_ID" => "BitrixMobile", //Идентификатор приложения
        "MESSAGE" => "У нас сегодня акция! Не пропусти!",
        "EXPIRY" => 0, //время жизни уведомления на сервере Apple и Google
        "PARAMS"=>array("redirect"=>"/events/"),
        "BADGE" => 1 //счетчик на иконке приложения
    );


    $arMessages[] = $message;
    $manager = new CPushManager();
    $manager->SendMessage($arMessages);
}
?>
<script>
var lastNotification = BXMobileApp.PushManager.getLastNotification();
//alert(lastNotification);
</script>
<script>
    //$.fancybox.open('<div class="message"><h1>Добавлен отчёт!</h1></div>');
    BX.addCustomEvent("onPullEvent", function(module_id,command,params) {
        if (module_id == "test" && command == 'check')
        {
            alert(params);
            console.log('Work!');
        }
    });
</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>