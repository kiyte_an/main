<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Конструктор");
?>
<h1>Конструктор</h1>
<p>
Для создания нового склада нажмите "Создать новый склад" и заполните все необходимые поля.
</p>
<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">
  Создать новый склад
</button>
<?
//pr ($_SERVER);
?>
<!-- Modal -->
<div class="modal fade new-warehouse-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Новый склад</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=$_SERVER['REQUEST_URI']?>" class="new-warehouse-form">
          <div class="form-group">
            <label for="warehouse-name" class="col-form-label">Название:</label>
            <input type="text" class="form-control" id="warehouse-name">
          </div>
          <div class="form-group">
            <label for="warehouse-width" class="col-form-label">Ширина склада:</label>
            <input type="number" class="form-control" id="warehouse-width">
          </div>
		  <div class="form-group">
            <label for="warehouse-height" class="col-form-label">Высота склада:</label>
            <input type="number" class="form-control" id="warehouse-height">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" name="save" class="btn btn-success">Сохранить</button>
      </div>
    </div>
  </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>