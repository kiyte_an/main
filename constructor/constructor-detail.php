<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("constructor-detail");
?>
<h1>Склад</h1>
<p>
Для разметки склада воспользуйтесь мастером создания нитки, для редактирования склада - нажмите редактировать.
</p>
	<div class="navbar constructor-navbar">
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal1">
			<i class="material-icons">auto_awesome_mosaic</i> Мастер создания нитки
		</button>
		<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal3">
			<i class="material-icons">settings</i> Редактировать склад
		</button>
	</div>

<div id="table-container">
	<table class="warehouse-layout">
		<tbody>
			<tr>
				<td class="td-item" id="cell0_0" data-row="0" data-col="0" height="158.3"><span
						class="cell-item type1 item_num_8256 triple" data-row="0" data-col="0" data-id="8256"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_1" data-row="0" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell0_2" data-row="0" data-col="2" height="158.3"><span
						class="cell-item type1 item_num_8257 triple" data-row="0" data-col="2" data-id="8257"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_3" data-row="0" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell0_4" data-row="0" data-col="4" height="158.3"><span
						class="cell-item type1 item_num_8258 dual" data-row="0" data-col="4" data-id="8258"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_5" data-row="0" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell0_6" data-row="0" data-col="6" height="158.3"><span
						class="cell-item type1 item_num_8259 triple-row" data-row="0" data-col="6" data-id="8259"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_7" data-row="0" data-col="7" height="158.3"><span
						class="cell-item type1 item_num_8259" data-row="0" data-col="7" data-id="8259" data-first="1"></span>
				</td>
				<td class="td-item" id="cell0_8" data-row="0" data-col="8" height="158.3"><span
						class="cell-item type1 item_num_8259 triple-row-and" data-row="0" data-col="8" data-id="8259"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_9" data-row="0" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell1_0" data-row="1" data-col="0" height="158.3"><span
						class="cell-item type1 item_num_8256" data-row="1" data-col="0" data-id="8256" data-first="1"></span>
				</td>
				<td class="td-item" id="cell1_1" data-row="1" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell1_2" data-row="1" data-col="2" height="158.3"><span
						class="cell-item type1 item_num_8257" data-row="1" data-col="2" data-id="8257" data-first="1"></span>
				</td>
				<td class="td-item" id="cell1_3" data-row="1" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell1_4" data-row="1" data-col="4" height="158.3"><span
						class="cell-item type1 item_num_8258 dual-and" data-row="1" data-col="4" data-id="8258"
						data-first="1"></span></td>
				<td class="td-item" id="cell1_5" data-row="1" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell1_6" data-row="1" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell1_7" data-row="1" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell1_8" data-row="1" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell1_9" data-row="1" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell2_0" data-row="2" data-col="0" height="158.3"><span
						class="cell-item type1 item_num_8256 triple-and" data-row="2" data-col="0" data-id="8256"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_1" data-row="2" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell2_2" data-row="2" data-col="2" height="158.3"><span
						class="cell-item type1 item_num_8257 triple-and" data-row="2" data-col="2" data-id="8257"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_3" data-row="2" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell2_4" data-row="2" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell2_5" data-row="2" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell2_6" data-row="2" data-col="6" height="158.3"><span
						class="cell-item type1 item_num_8260 dual-row" data-row="2" data-col="6" data-id="8260"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_7" data-row="2" data-col="7" height="158.3"><span
						class="cell-item type1 item_num_8260 dual-row-and" data-row="2" data-col="7" data-id="8260"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_8" data-row="2" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell2_9" data-row="2" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell3_0" data-row="3" data-col="0" height="158.3"><span class="cell-item type1 triple"></span></td>
				<td class="td-item" id="cell3_1" data-row="3" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell3_2" data-row="3" data-col="2" height="158.3"></td>
				<td class="td-item" id="cell3_3" data-row="3" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell3_4" data-row="3" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell3_5" data-row="3" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell3_6" data-row="3" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell3_7" data-row="3" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell3_8" data-row="3" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell3_9" data-row="3" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell4_0" data-row="4" data-col="0" height="158.3"><span class="cell-item type1"></span></td>
				<td class="td-item" id="cell4_1" data-row="4" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell4_2" data-row="4" data-col="2" height="158.3"></td>
				<td class="td-item" id="cell4_3" data-row="4" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell4_4" data-row="4" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell4_5" data-row="4" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell4_6" data-row="4" data-col="6" height="158.3"><span
						class="cell-item type1 item_num_8261 dual-row" data-row="4" data-col="6" data-id="8261"
						data-first="1"></span></td>
				<td class="td-item" id="cell4_7" data-row="4" data-col="7" height="158.3"><span
						class="cell-item type1 item_num_8261 dual-row-and" data-row="4" data-col="7" data-id="8261"
						data-first="1"></span></td>
				<td class="td-item" id="cell4_8" data-row="4" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell4_9" data-row="4" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell5_0" data-row="5" data-col="0" height="158.3"><span class="cell-item type1 triple-and"></span></td>
				<td class="td-item" id="cell5_1" data-row="5" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell5_2" data-row="5" data-col="2" height="158.3"></td>
				<td class="td-item" id="cell5_3" data-row="5" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell5_4" data-row="5" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell5_5" data-row="5" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell5_6" data-row="5" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell5_7" data-row="5" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell5_8" data-row="5" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell5_9" data-row="5" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell6_0" data-row="6" data-col="0" height="158.3"></td>
				<td class="td-item" id="cell6_1" data-row="6" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell6_2" data-row="6" data-col="2" height="158.3"></td>
				<td class="td-item" id="cell6_3" data-row="6" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell6_4" data-row="6" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell6_5" data-row="6" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell6_6" data-row="6" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell6_7" data-row="6" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell6_8" data-row="6" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell6_9" data-row="6" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell7_0" data-row="7" data-col="0" height="158.3"></td>
				<td class="td-item" id="cell7_1" data-row="7" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell7_2" data-row="7" data-col="2" height="158.3"></td>
				<td class="td-item" id="cell7_3" data-row="7" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell7_4" data-row="7" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell7_5" data-row="7" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell7_6" data-row="7" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell7_7" data-row="7" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell7_8" data-row="7" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell7_9" data-row="7" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell8_0" data-row="8" data-col="0" height="158.3"></td>
				<td class="td-item" id="cell8_1" data-row="8" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell8_2" data-row="8" data-col="2" height="158.3"></td>
				<td class="td-item" id="cell8_3" data-row="8" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell8_4" data-row="8" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell8_5" data-row="8" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell8_6" data-row="8" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell8_7" data-row="8" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell8_8" data-row="8" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell8_9" data-row="8" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell9_0" data-row="9" data-col="0" height="158.3"></td>
				<td class="td-item" id="cell9_1" data-row="9" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell9_2" data-row="9" data-col="2" height="158.3"></td>
				<td class="td-item" id="cell9_3" data-row="9" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell9_4" data-row="9" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell9_5" data-row="9" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell9_6" data-row="9" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell9_7" data-row="9" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell9_8" data-row="9" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell9_9" data-row="9" data-col="9" height="158.3"></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="card table-responsive">
	<table class="table total-table">
		<thead>
			<tr>
				<td>Нитка</td>
				<td>Адрес</td>
				<td>Редактировать</td>
				<td>Копировать</td>
				<td>Переместить</td>
				<td>Удалить</td>
			<tr>
		</thead>
		<tbody>
			<tr>
				<td>Нитка 1</td>
				<td>1x1</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<tr>
				<td>Нитка 2</td>
				<td>3x1</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<tr>
				<td>Нитка 3</td>
				<td>5x1</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<tr>
				<td>Нитка 4</td>
				<td>7x1</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<tr>
				<td>Нитка 5</td>
				<td>7x3</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<tr>
				<td>Нитка 6</td>
				<td>7x5</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
		<div class="form-group">
			<label for="exampleFormControlSelect1">Направление</label>
			<select class="form-control" id="exampleFormControlSelect1">
			  <option>Горизонтально</option>
			  <option>Вертикально</option>
			</select>
		  </div>
		  <div class="form-group">
            <label for="sections-num" class="col-form-label">Количество секций:</label>
            <input type="number" class="form-control" id="sections-num">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal2">Далее</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
<div class="sections-list">
	<div class="item"><!--Количетво данных элементов зависит от значения заданного в предыдущем попапе, поле Количество секций-->
		<div class="title">Секция N</div> <!--Вместо N порядковый номер секции-->
		<div class="pallet-spaces">
			<label>Количество паллетомест:</label>
  <input class="form-check-input" type="radio" name="pallet-spaces" id="pallet-spaces-1" value="pallet-spaces-1">
  <label class="form-check-label" for="pallet-spaces-1">
    1
  </label>
<input class="form-check-input" type="radio" name="pallet-spaces" id="pallet-spaces-2" value="pallet-spaces-2">
  <label class="form-check-label" for="pallet-spaces-2">
    2
  </label>
  <input class="form-check-input" type="radio" name="pallet-spaces" id="pallet-spaces-3" value="pallet-spaces-3" checked>
  <label class="form-check-label" for="pallet-spaces-3">
    3
  </label>
<input class="form-check-input" type="radio" name="pallet-spaces" id="pallet-spaces-4" value="pallet-spaces-4">
  <label class="form-check-label" for="pallet-spaces-4">
    4
  </label>
		</div>
	</div>
</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Редактирование склада</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="warehouse-name" class="col-form-label">Название:</label>
            <input type="text" class="form-control" id="warehouse-name">
          </div>
          <div class="form-group">
            <label for="warehouse-width" class="col-form-label">Ширина склада:</label>
            <input type="number" class="form-control" id="warehouse-width">
          </div>
		  <div class="form-group">
            <label for="warehouse-height" class="col-form-label">Высота склада:</label>
            <input type="number" class="form-control" id="warehouse-height">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>