<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("new-warehouse");
?>
<div id="warehouse-container">
	<div class="warehouse-body">
		<div class="item rack" data-row="0" data-col="0"></div><div class="item crossbar" data-row="0" data-col="1"></div><div class="item crossbar" data-row="0" data-col="2"></div>
		<div class="item rack" data-row="0" data-col="3"></div><div class="item" data-row="0" data-col="4"></div><div class="item" data-row="0" data-col="5"></div>
		<div class="item" data-row="0" data-col="6"></div><div class="item" data-row="0" data-col="7"></div><div class="item rack" data-row="0" data-col="8"></div>
		<div class="item crossbar" data-row="0" data-col="9"></div><div class="item crossbar" data-row="0" data-col="10"></div><div class="item rack" data-row="0" data-col="11"></div>
		<div class="item" data-row="0" data-col="12"></div><!--сторока 1-->
		<div class="item crossbar" data-row="1" data-col="0"></div><div class="item pallet-space p1" data-row="1" data-col="1"></div><div class="item pallet-space p2" data-row="1" data-col="2"></div>
		<div class="item crossbar" data-row="1" data-col="3"></div><div class="item" data-row="1" data-col="4"></div><div class="item" data-row="1" data-col="5"></div>
		<div class="item" data-row="1" data-col="6"></div><div class="item" data-row="1" data-col="7"></div><div class="item crossbar" data-row="1" data-col="8"></div>
		<div class="item pallet-space p1" data-row="1" data-col="9"></div><div class="item pallet-space p2" data-row="1" data-col="10"></div><div class="item crossbar" data-row="1" data-col="11"></div>
		<div class="item" data-row="1" data-col="12"></div><!--сторока 2-->
		<div class="item crossbar" data-row="2" data-col="0"></div><div class="item pallet-space p2" data-row="2" data-col="1"></div><div class="item pallet-space p1" data-row="2" data-col="2"></div>
		<div class="item crossbar" data-row="2" data-col="3"></div><div class="item" data-row="2" data-col="4"></div><div class="item" data-row="2" data-col="5"></div>
		<div class="item" data-row="2" data-col="6"></div><div class="item" data-row="2" data-col="7"></div><div class="item crossbar" data-row="2" data-col="8"></div>
		<div class="item pallet-space p2" data-row="2" data-col="9"></div><div class="item pallet-space p1" data-row="2" data-col="10"></div><div class="item crossbar" data-row="2" data-col="11"></div>
		<div class="item" data-row="2" data-col="12"></div><!--сторока 3-->
		<div class="item crossbar" data-row="3" data-col="0"></div><div class="item pallet-space p1" data-row="3" data-col="1"></div><div class="item pallet-space p2" data-row="3" data-col="2"></div>
		<div class="item crossbar" data-row="3" data-col="3"></div><div class="item" data-row="3" data-col="4"></div><div class="item" data-row="3" data-col="5"></div>
		<div class="item" data-row="3" data-col="6"></div><div class="item" data-row="3" data-col="7"></div><div class="item crossbar" data-row="3" data-col="8"></div>
		<div class="item pallet-space p1" data-row="3" data-col="9"></div><div class="item pallet-space p2" data-row="3" data-col="10"></div><div class="item crossbar" data-row="3" data-col="11"></div>
		<div class="item" data-row="3" data-col="12"></div><!--сторока 4-->
		<div class="item crossbar" data-row="4" data-col="0"></div><div class="item pallet-space p2" data-row="4" data-col="1"></div><div class="item pallet-space p1" data-row="4" data-col="2"></div>
		<div class="item crossbar" data-row="4" data-col="3"></div><div class="item" data-row="4" data-col="4"></div><div class="item" data-row="4" data-col="5"></div>
		<div class="item" data-row="4" data-col="6"></div><div class="item" data-row="4" data-col="7"></div><div class="item crossbar" data-row="4" data-col="8"></div>
		<div class="item pallet-space p2" data-row="4" data-col="9"></div><div class="item pallet-space p1" data-row="4" data-col="10"></div><div class="item crossbar" data-row="4" data-col="11"></div>
		<div class="item" data-row="4" data-col="12"></div><!--сторока 5-->
		<div class="item rack" data-row="5" data-col="0"></div><div class="item crossbar" data-row="5" data-col="1"></div><div class="item crossbar" data-row="5" data-col="2"></div>
		<div class="item rack" data-row="5" data-col="3"></div><div class="item rack" data-row="5" data-col="4"></div><div class="item crossbar" data-row="5" data-col="5"></div>
		<div class="item crossbar" data-row="5" data-col="6"></div><div class="item rack" data-row="5" data-col="7"></div><div class="item rack" data-row="5" data-col="8"></div>
		<div class="item crossbar" data-row="5" data-col="9"></div><div class="item crossbar" data-row="5" data-col="10"></div><div class="item rack" data-row="5" data-col="11"></div>
		<div class="item" data-row="5" data-col="12"></div><!--сторока 6-->
		<div class="item crossbar" data-row="6" data-col="0"></div><div class="item pallet-space p1" data-row="6" data-col="1"></div><div class="item pallet-space p2" data-row="6" data-col="2"></div>
		<div class="item crossbar" data-row="6" data-col="3"></div><div class="item crossbar" data-row="6" data-col="4"></div><div class="item pallet-space p1" data-row="6" data-col="5"></div>
		<div class="item pallet-space p2" data-row="6" data-col="6"></div><div class="item crossbar" data-row="6" data-col="7"></div><div class="item" data-row="6" data-col="8"></div>
		<div class="item" data-row="6" data-col="9"></div><div class="item" data-row="6" data-col="10"></div><div class="item" data-row="6" data-col="11"></div>
		<div class="item" data-row="6" data-col="12"></div><!--сторока 7-->
		<div class="item crossbar" data-row="7" data-col="0"></div><div class="item pallet-space p2" data-row="7" data-col="1"></div><div class="item pallet-space p1" data-row="7" data-col="2"></div>
		<div class="item crossbar" data-row="7" data-col="3"></div><div class="item crossbar" data-row="7" data-col="4"></div><div class="item pallet-space p2" data-row="7" data-col="5"></div>
		<div class="item pallet-space p1" data-row="7" data-col="6"></div><div class="item crossbar" data-row="7" data-col="7"></div><div class="item" data-row="7" data-col="8"></div>
		<div class="item" data-row="7" data-col="9"></div><div class="item" data-row="7" data-col="10"></div><div class="item" data-row="7" data-col="11"></div>
		<div class="item" data-row="7" data-col="12"></div><!--сторока 8-->
		<div class="item crossbar" data-row="8" data-col="0"></div><div class="item pallet-space p1" data-row="8" data-col="1"></div><div class="item pallet-space p2" data-row="8" data-col="2"></div>
		<div class="item crossbar" data-row="8" data-col="3"></div><div class="item crossbar" data-row="8" data-col="4"></div><div class="item pallet-space p1" data-row="8" data-col="5"></div>
		<div class="item pallet-space p2" data-row="8" data-col="6"></div><div class="item crossbar" data-row="8" data-col="7"></div><div class="item" data-row="8" data-col="8"></div>
		<div class="item" data-row="8" data-col="9"></div><div class="item" data-row="8" data-col="10"></div><div class="item" data-row="8" data-col="11"></div>
		<div class="item" data-row="8" data-col="12"></div><!--сторока 9-->
		<div class="item crossbar" data-row="9" data-col="0"></div><div class="item pallet-space p2" data-row="9" data-col="1"></div><div class="item pallet-space p1" data-row="9" data-col="2"></div>
		<div class="item crossbar" data-row="9" data-col="3"></div><div class="item crossbar" data-row="9" data-col="4"></div><div class="item pallet-space p2" data-row="9" data-col="5"></div>
		<div class="item pallet-space p1" data-row="9" data-col="6"></div><div class="item crossbar" data-row="9" data-col="7"></div><div class="item" data-row="9" data-col="8"></div>
		<div class="item" data-row="9" data-col="9"></div><div class="item" data-row="9" data-col="10"></div><div class="item" data-row="9" data-col="11"></div>
		<div class="item" data-row="9" data-col="12"></div><!--сторока 10-->
		<div class="item crossbar" data-row="10" data-col="0"></div><div class="item pallet-space p1" data-row="10" data-col="1"></div><div class="item pallet-space p2" data-row="10" data-col="2"></div>
		<div class="item crossbar" data-row="10" data-col="3"></div><div class="item crossbar" data-row="10" data-col="4"></div><div class="item pallet-space p1" data-row="10" data-col="5"></div>
		<div class="item pallet-space p2" data-row="10" data-col="6"></div><div class="item crossbar" data-row="10" data-col="7"></div><div class="item" data-row="10" data-col="8"></div>
		<div class="item" data-row="10" data-col="9"></div><div class="item" data-row="10" data-col="10"></div><div class="item" data-row="10" data-col="11"></div>
		<div class="item" data-row="10" data-col="12"></div><!--сторока 11-->
		<div class="item crossbar" data-row="11" data-col="0"></div><div class="item pallet-space p2" data-row="11" data-col="1"></div><div class="item pallet-space p1" data-row="11" data-col="2"></div>
		<div class="item crossbar" data-row="11" data-col="3"></div><div class="item crossbar" data-row="11" data-col="4"></div><div class="item pallet-space p2" data-row="11" data-col="5"></div>
		<div class="item pallet-space p1" data-row="11" data-col="6"></div><div class="item crossbar" data-row="11" data-col="7"></div><div class="item" data-row="11" data-col="8"></div>
		<div class="item" data-row="11" data-col="9"></div><div class="item" data-row="11" data-col="10"></div><div class="item" data-row="11" data-col="11"></div>
		<div class="item" data-row="11" data-col="12"></div><!--сторока 12-->
		<div class="item rack" data-row="12" data-col="0"></div><div class="item crossbar" data-row="12" data-col="1"></div><div class="item crossbar" data-row="12" data-col="2"></div>
		<div class="item rack" data-row="12" data-col="3"></div><div class="item rack" data-row="12" data-col="4"></div><div class="item crossbar" data-row="12" data-col="5"></div>
		<div class="item crossbar" data-row="12" data-col="6"></div><div class="item rack" data-row="12" data-col="7"></div><div class="item" data-row="12" data-col="8"></div>
		<div class="item" data-row="12" data-col="9"></div><div class="item" data-row="12" data-col="10"></div><div class="item" data-row="12" data-col="11"></div>
		<div class="item" data-row="12" data-col="12"></div><!--сторока 13-->
	</div>
</div>
<script>
	let container;
	let items = document.querySelectorAll('.item');
	let numColums = 13;//Количество столбцов задается пользователем
	let itemSize;

	function sizeContainer(){
		container = document.getElementById('warehouse-container').clientWidth;
		itemSize = container/numColums;
		for(let item of items){
			item.style.width = itemSize+'px';
			item.style.height = itemSize+'px';
		}
	}
	sizeContainer();
	window.addEventListener('resize', function(event){
	  sizeContainer();
	});
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>