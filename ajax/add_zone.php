<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if (intval ($request->get('storeID')) == 0) return;


$ID = $request->get('storeID');


CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');


$arFilter   = ["IBLOCK_ID" => CFG_IBLOCK_STORES_ID, "SECTION_ID" => $ID];

$sections = helper::getIblockSectionsIB ($arFilter);

$zoneNum = count ($sections) + 1;

$zoneName = CFG_ZONE_NAME . ' ' . $zoneNum;

$bs = new CIBlockSection;
$arFields = [
	"ACTIVE" 			=> "Y",
	"IBLOCK_ID" 		=> CFG_IBLOCK_STORES_ID,
	"NAME" 				=> $zoneName,
	"SORT"	 			=> 500,
	"IBLOCK_SECTION_ID" => $ID
];

$ID_Zone = $bs->Add($arFields);



$ReturnData = array(
	'SUCCESS'	=> 1
);

echo json_encode($ReturnData);
exit();

?>