<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (intval ($_POST['SECTION_ID']) == 0 || intval ($_POST['SHELF_ID'])==0) return;

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');


$dbItems = \Bitrix\Iblock\SectionTable::getList(array(
	'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
	'filter' => array('IBLOCK_ID' => CFG_IBLOCK_STORES_ID, 'ID' => $_POST['SECTION_ID'])
));


$store = $dbItems->fetch();

if (!$store) return;


$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         => CFG_IBLOCK_REPORTS_ID, 
        '=PROPERTY_STORE'   => $_POST['SECTION_ID'], 
        '=PROPERTY_STATUS'  => $GLOBALS ['STATUS']['open'], 
        "ACTIVE_DATE"       => "Y", 
        "ACTIVE"            => "Y"];
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $report = $arFields;
    
}

if (!$report)
{
	$ReturnData = array(
		'SUCCESS'	=> 1,
		'REPORT'	=> 0
		);

		echo json_encode($ReturnData);
		exit();

}

$arShelvs = [];

$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         => CFG_IBLOCK_DAMAGE_REPORT_ID, 
        '=PROPERTY_REPORT'   => $report['ID'], 
		'=PROPERTY_SHELVING'   => $_POST['SHELF_ID'], 
        "ACTIVE_DATE"       => "Y", 
        "ACTIVE"            => "Y"];
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$damages = [];

while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;

	$shelvID = $arFields['PROPERTIES']['SHELVING']['VALUE'];

	if (!empty($damages[$shelvID]))
	{
		$arFields ['SHELV'] = $arShelvs[$shelvID];
	} else
	{
		$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
		$arFilter = [
        '=ID' 				=> $shelvID, 
        "ACTIVE_DATE"       => "Y", 
        "ACTIVE"            => "Y"];
		$res2 = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
		$ob2 = $res2->GetNextElement();
		$arFields2 = $ob2->GetFields();
		$arProps2 = $ob2->GetProperties();
		$arFields2['PROPERTIES'] = $arProps2;
		$shelv = $arFields2;
		$arShelvs[$shelvID] = $shelv;
		$arFields ['SHELV'] = $shelv;
	}

    $damages[] = $arFields;
    
}

$out = [];

$num = 0;

foreach ($damages as $_damage)
{
	$num++;
	$prioritet = $_damage['PROPERTIES']['PRIORITY']['VALUE'];
	$address = '(' . $_damage['SHELV']['PROPERTIES']['CELL1_ROW']['VALUE'] . ',' . $_damage['SHELV']['PROPERTIES']['CELL1_COLUM']['VALUE'] . ') - (' . $_damage['SHELV']['PROPERTIES']['CELL3_ROW']['VALUE'] . ',' . $_damage['SHELV']['PROPERTIES']['CELL3_COLUM']['VALUE'] . ')';

	$damage_id =  $_damage['PROPERTIES']['DAMAGE']['VALUE'];

	$arSelect = ["ID", "IBLOCK_ID", "NAME", "IBLOCK_SECTION_ID", "DATE_ACTIVE_FROM","PROPERTY_*"];
	$arFilter = [
        'ID'         => $damage_id
	];
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
	$ob = $res->GetNextElement();

   	$arFields = $ob->GetFields();  
	$damage = $arFields;
	
	$dbItems = \Bitrix\Iblock\SectionTable::getList(array(
		'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
		'filter' => array('ID' => $damage['IBLOCK_SECTION_ID'])
	));
	
	$damage_section = $dbItems->fetch();

	if (!empty ($damage_section['NAME']))
	{
		$damage_text = $damage_section['NAME'] . ' - ' . $damage['NAME'];
	} else
	{
		$damage_text = $damage['NAME'];
	}
	
	$out[] = 
	[
		'NUM' 		=> $num,
		'ADDRESS'	=> $address,
		'PRIORITET'	=> $prioritet,
		'DAMAGE'	=> $damage_text,
	];
}

//pr ($out);


$ReturnData = array(
	'SUCCESS'	=> 1,
	'REPORT'	=> 1,
	'DAMAGE'	=> $out
	);

	echo json_encode($ReturnData);
	exit();
?>