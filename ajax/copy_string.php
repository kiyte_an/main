<?
//exit;
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if (intval ($request->get('ID')) == 0) return;

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "IBLOCK_SECTION_ID", "MODIFIED_BY", "ACTIVE", "PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'     	=> CFG_IBLOCK_STRINGS_ID,
        '=ID'			=> $request->get('ID')
];
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $string = $arFields;
    
}

if (empty($string)) return false;

if ($string['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID'] == $GLOBALS ['DIRECTION']['horizontal'])
{
	$string['PROPERTIES']['BEGIN_POSITION_Y']['VALUE'] += 1;
} else
{
	$string['PROPERTIES']['BEGIN_POSITION_X']['VALUE'] += 1;
}


$el = new CIBlockElement;

$PROP = [];

foreach ($string['PROPERTIES'] as $key => $_prop)
{
	if ($_prop['PROPERTY_TYPE'] == 'L')
	{
		$PROP [$_prop['ID']] = $_prop ['VALUE_ENUM_ID'];
	} else
	{
		$PROP [$_prop['ID']] = $_prop ['VALUE'];
	}
}




$arLoadProductArray = Array(
	"MODIFIED_BY" 	 	=> $string["MODIFIED_BY"], 
	"IBLOCK_SECTION_ID"	=> $string["IBLOCK_SECTION_ID"],
	"IBLOCK_ID"			=> $string["IBLOCK_ID"],
	"PROPERTY_VALUES"	=> $PROP,
	"NAME" 				=> $string["NAME"] . '_copy',
	"ACTIVE"   			=> $string["ACTIVE"],
);

$storeID = $string ['PROPERTIES']['STORE']['VALUE'];

//$start_x 		= ($PROP[CFG_PROP_DIRECTION_ID] == $GLOBALS ['DIRECTION']['horizontal']) ? $PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID] : ($PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID] + 1);
//$start_y 		= ($PROP[CFG_PROP_DIRECTION_ID] == $GLOBALS ['DIRECTION']['horizontal']) ? ($PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID] + 1) : $PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID];

$start_x 		= $PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID];
$start_y 		= $PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID];

$direction 		= $PROP[CFG_PROP_DIRECTION_ID];
$num_sections	= $PROP[CFG_PROP_NUM_SECTIONS_ID];

$arFields = [
		'X'				=> $start_x,
		'Y' 				=> $start_y,
		'DIRECTION' 	=> $direction,
		'NUM_SECTIONS' 	=> $num_sections,
];

test_store($storeID, $arFields);
$report ['ID'] = $el->Add($arLoadProductArray);


$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "IBLOCK_SECTION_ID", "MODIFIED_BY", "ACTIVE", "PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         		=> CFG_IBLOCK_STORES_ID,
        'PROPERTY_STRING'			=> $string['ID']
];

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$stellage = [];

while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $stellage[] = $arFields;
} 

$movePallets = [];


foreach ($stellage as $key => $item)
{
	$el = new CIBlockElement;

	$PROP = [];

	foreach ($item['PROPERTIES'] as $key => $_prop)
	{
		if ($_prop['CODE'] == 'STRING')
		{
			$PROP [$_prop['ID']] = $report ['ID'];
		}
		else if ($_prop['PROPERTY_TYPE'] == 'L')
		{
			$PROP [$_prop['ID']] = $_prop ['VALUE_ENUM_ID'];
		} else
		{
			$PROP [$_prop['ID']] = $_prop ['VALUE'];
		}
	}


	if ($string['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID'] == $GLOBALS ['DIRECTION']['horizontal'])
	{
		$PROP[CFG_PROP_CELL1_ROW_ID] += 1;
		$PROP[CFG_PROP_CELL2_ROW_ID] += 1;
		$PROP[CFG_PROP_CELL3_ROW_ID] += 1;
		$PROP[CFG_PROP_CELL4_ROW_ID] += 1;
	} else
	{
		$PROP[CFG_PROP_CELL1_COLUM_ID] += 1;
		$PROP[CFG_PROP_CELL2_COLUM_ID] += 1;
		$PROP[CFG_PROP_CELL3_COLUM_ID] += 1;
		$PROP[CFG_PROP_CELL4_COLUM_ID] += 1;
	}
	$arLoadProductArray = Array(
		"MODIFIED_BY" 	 	=> $item["MODIFIED_BY"], 
		"IBLOCK_SECTION_ID"	=> $item["IBLOCK_SECTION_ID"],
		"IBLOCK_ID"			=> $item["IBLOCK_ID"],
		"PROPERTY_VALUES"	=> $PROP,
		"NAME" 				=> $item["NAME"] . '_copy',
		"ACTIVE"   			=> $item["ACTIVE"],
	);

//	pr ($arLoadProductArray);

	$report2['ID'][] = $repID = $el->Add($arLoadProductArray);
	//$lastNum = count ($report2['ID']) - 1;

	if ($PROP[CFG_PROP_CELL1_ROW_ID] > 0 && $PROP[CFG_PROP_CELL1_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL1_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL1_ROW_ID], 'ID' => $repID];
	}
	
	if ($PROP[CFG_PROP_CELL2_ROW_ID] > 0 && $PROP[CFG_PROP_CELL2_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL2_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL2_ROW_ID], 'ID' => $repID];
	}
	if ($PROP[CFG_PROP_CELL3_ROW_ID] > 0 && $PROP[CFG_PROP_CELL3_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL3_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL3_ROW_ID], 'ID' => $repID];
	}
	if ($PROP[CFG_PROP_CELL4_ROW_ID] > 0 && $PROP[CFG_PROP_CELL4_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL4_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL4_ROW_ID], 'ID' => $repID];
	}
}


$ReturnData = array(
	'SUCCESS'			=> 1,
	'NAME'				=> $string["NAME"] . '_copy',
	'ID'				=> $report ['ID'],
	'NUM_SECTIONS'		=> $string['PROPERTIES']['NUM_SECTIONS']['VALUE'],
	'BEGIN_POSITION_X'	=> $string['PROPERTIES']['BEGIN_POSITION_X']['VALUE'],
	'BEGIN_POSITION_Y'	=> $string['PROPERTIES']['BEGIN_POSITION_Y']['VALUE'],
	'MOVE_PALLETS'	=> $movePallets
);

echo json_encode($ReturnData);
exit();

?>