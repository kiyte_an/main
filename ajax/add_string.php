<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if (empty ($request->get('PROP')) || intval ($request->get('warehouse_id')) == 0 || intval ($request->get('direction'))==0) return;


$id = $request->get('id');


CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

$el = new CIBlockElement;

//pr ($_GET);
//exit;

if ($id > 0)
{
	$PROP = [];
	$PROP[CFG_PROP_DIRECTION_ID] = $request->get('direction');
	$el->Update($id, ["PROPERTY_VALUES" => $PROP]);

	$ReturnData = array(
		'SUCCESS'	=> 1
	);

} else
{


	$arSelect   = ["ID", "NAME", "IBLOCK_ID"];
	$arFilter   = ["IBLOCK_ID" => CFG_IBLOCK_STORES_ID, "SECTION_ID" => $request->get('warehouse_id')];
	$arOrder    = ["ID" => "ASC"];
	$res = CIBlockSection::GetList($arOrder, $arFilter,true,$arSelect);
	
	$out = [];

	$arSectZone = $res->GetNext();
	

	$PROP = [];
	$PROP[CFG_PROP_DIRECTION_ID] = $request->get('direction');
	$PROP[CFG_PROP_STRING_TYPE_ID] = $request->get('stringType');
	$PROP[CFG_PROP_STRING_STORE_ID] = $request->get('warehouse_id'); 
	$PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID] = $request->get('string_begin_position_x');
	$PROP[CFG_PROP_STRING_BEGIN_POSITION_Y_ID] = $request->get('string_begin_position_y');
	$PROP[CFG_PROP_NUM_SECTIONS_ID] = count ($request->get('PROP'));
	$PROP[CFG_PROP_ZONE_ID] = $arSectZone['ID'];
	
	
	
	
	
	$arLoadProductArray = Array(
	  "MODIFIED_BY"    => $USER->GetID(), 
	  "IBLOCK_SECTION_ID" => false,
	  "IBLOCK_ID"      => CFG_IBLOCK_STRINGS_ID,
	  "PROPERTY_VALUES"=> $PROP,
	  "NAME"           => $request->get('string_name'),
	  "ACTIVE"         => "Y",
	  );
	

	//AddMessage2Log($arLoadProductArray, '$arLoadProductArray');


	  $storeID = $request->get('warehouse_id');

		$start_x 		= $PROP[CFG_PROP_STRING_BEGIN_POSITION_X_ID];
		$start_y 		= $PROP[CFG_PROP_STRING_BEGIN_POSITION_Y_ID];
		$direction 		= $PROP[CFG_PROP_DIRECTION_ID];
		$stringType 		= $PROP[CFG_PROP_STRING_TYPE_ID];
		$num_sections	= $PROP[CFG_PROP_NUM_SECTIONS_ID];

	  $arFields = [
			'X'				=> $start_x,
			'Y' 			=> $start_y,
			'DIRECTION' 	=> $direction,
			'TYPE'	 	=> $stringType,
			'NUM_SECTIONS' 	=> $num_sections,
		];

	test_store($storeID, $arFields);
	$report ['ID'] = $el->Add($arLoadProductArray);

	if (!$report ['ID'])
	{
		$ReturnData = array(
			'SUCCESS'			=> 0,
			'ERROR'				=> 1,
			'MSG'				=> 'Ошибка добавления: нитка не была создана',
		);
		echo json_encode($ReturnData);
		exit();
	}

	$p_x = $request->get('string_begin_position_x');
	$p_y = $request->get('string_begin_position_y');
	$direction = $request->get('direction');
	$stringType = $request->get('stringType');
	$pallets_num = $request->get('pallets_num');



if ($stringType	== $GLOBALS ['TYPE']['wall'])
{
	$el2 = new CIBlockElement;

	$outPallets = [];

	$PROP2 = [
		CFG_PROP_STRING_ID						=> $report ['ID'],
		CFG_PROP_NUM_PALLETS_ID				=> $pallets_num,
	];

	$arLoadProductArray2 = Array(
		"MODIFIED_BY"    => $USER->GetID(),
		"IBLOCK_SECTION_ID" => $request->get('warehouse_id'),
		"IBLOCK_ID"      => CFG_IBLOCK_STORES_ID,
		"PROPERTY_VALUES"=> $PROP2,
		"NAME"           => 'Палет_' . $report ['ID'] . '_0',
		"ACTIVE"         => "Y",
		);
  
		AddMessage2Log($arLoadProductArray2, '$arLoadProductArray2');
		//		pr ($arLoadProductArray2);
	  $stelage ['ID'] = $el2->Add($arLoadProductArray2);

		$x_pos = $p_x;
		$y_pos = $p_y;

	for ($i = 0; $i < $pallets_num; $i++)
	{
		$class = ' wallBlock';

		$outPallets [] = ['X' => $x_pos, 'Y' => $y_pos, 'ID' => $stelage ['ID'], 'CLASS' => $class];

		if ($request->get('direction') == $GLOBALS ['DIRECTION']['horizontal'])
		{
			$x_pos += 1;
		} else
		{
			$y_pos += 1;
		}

	}
	
} else
{

	$outPallets = [];

	foreach ($request->get('PROP') as $key => $item)
	{
		$el2 = new CIBlockElement;

	

		$PROP2 = [
			CFG_PROP_STRING_ID						=> $report ['ID'],
		];

		switch ($item) {
			case 1:
				$PROP2[CFG_PROP_CELL1_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL1_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;
			
				break;
			case 2:
				$PROP2[CFG_PROP_CELL1_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL1_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;

				$PROP2[CFG_PROP_CELL2_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL2_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;
				break;
			case 3:
				$PROP2[CFG_PROP_CELL1_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL1_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;

				$PROP2[CFG_PROP_CELL2_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL2_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;

				$PROP2[CFG_PROP_CELL3_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL3_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;

				break;
			case 4:
				$PROP2[CFG_PROP_CELL1_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL1_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;

				$PROP2[CFG_PROP_CELL2_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL2_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;

				$PROP2[CFG_PROP_CELL3_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL3_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;

				$PROP2[CFG_PROP_CELL4_ROW_ID] = $p_y;
				$PROP2[CFG_PROP_CELL4_COLUM_ID] = $p_x;
				if ($direction == $GLOBALS ['DIRECTION']['horizontal']) 
					$p_x++;
				else
					$p_y++;
				break;
			}
	


	


		$arLoadProductArray2 = Array(
			"MODIFIED_BY"    => $USER->GetID(),
			"IBLOCK_SECTION_ID" => $request->get('warehouse_id'),
			"IBLOCK_ID"      => CFG_IBLOCK_STORES_ID,
			"PROPERTY_VALUES"=> $PROP2,
			"NAME"           => 'Палет_' . $report ['ID'] . '_' . $key,
			"ACTIVE"         => "Y",
			);
	  
			//AddMessage2Log($arLoadProductArray2, '$arLoadProductArray2');
			//		pr ($arLoadProductArray2);
		  $stelage ['ID'] = $el2->Add($arLoadProductArray2);

		  if ($PROP2[CFG_PROP_CELL1_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL1_COLUM_ID] > 0)
		  {
			$class = ''; 
			$class = ($request->get('direction') == $GLOBALS ['DIRECTION']['horizontal']) ? ' triple-row' : ' triple';
	        if (intval($PROP2[CFG_PROP_CELL2_ROW_ID]) == 0 || intval ($PROP2[CFG_PROP_CELL2_COLUM_ID]) == 0)
    	    {
				$class2 = ($request->get('direction') == $GLOBALS ['DIRECTION']['horizontal']) ? ' triple-row-and' : ' triple-and';
            	$class .= ' ' . $class2;
	        }

			$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL1_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL1_ROW_ID], 'ID' => $stelage ['ID'], 'CLASS' => $class];
		  }
	  
		  if ($PROP2[CFG_PROP_CELL2_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL2_COLUM_ID] > 0)
		  {
				$class = '';
			if (intval($PROP2[CFG_PROP_CELL3_ROW_ID]) == 0 || intval ($PROP2[CFG_PROP_CELL3_COLUM_ID]) == 0)
        	{
				$class2 = ($request->get('direction') == $GLOBALS ['DIRECTION']['horizontal']) ? ' triple-row-and' : ' triple-and';
	            $class .= ' ' . $class2;
    	    }
			$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL2_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL2_ROW_ID], 'ID' => $stelage ['ID'], 'CLASS' => $class];
		  }
		  if ($PROP2[CFG_PROP_CELL3_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL3_COLUM_ID] > 0)
		  {
			$class = '';
			if (intval($PROP2[CFG_PROP_CELL4_ROW_ID]) == 0 || intval ($PROP2[CFG_PROP_CELL4_COLUM_ID]) == 0)
	        {
				$class2 = ($request->get('direction') == $GLOBALS ['DIRECTION']['horizontal']) ? ' triple-row-and' : ' triple-and';
        	    $class .= ' ' . $class2;
	        }
			$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL3_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL3_ROW_ID], 'ID' => $stelage ['ID'], 'CLASS' => $class];
		  }
		  if ($PROP2[CFG_PROP_CELL4_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL4_COLUM_ID] > 0)
		  {
			$class = '';
			$class2 = ($request->get('direction') == $GLOBALS ['DIRECTION']['horizontal']) ? ' triple-row-and' : ' triple-and';
	        $class .= ' ' . $class2;
			$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL4_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL4_ROW_ID], 'ID' => $stelage ['ID'], 'CLASS' => $class];
		  }

	}

}


//	$el->Update($report ['ID'], ["NAME" => 'Нитка_' . $report ['ID']]);
	$ReturnData = array(
		'SUCCESS'			=> 1,
		'NAME'				=> $request->get('string_name'),
		'ID'				=> $report ['ID'],
		'NUM_SECTIONS'		=> count ($request->get('PROP')),
		'BEGIN_POSITION_X'	=> $request->get('string_begin_position_x'),
		'BEGIN_POSITION_Y'	=> $request->get('string_begin_position_y'),
		'OUT_PALLETS'		=> $outPallets
	);
}	



	echo json_encode($ReturnData);
	exit();

?>