<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();


if (intval ($request->get('id')) == 0) return;

$id = $request->get('id');

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "IBLOCK_SECTION_ID", "MODIFIED_BY", "ACTIVE", "PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         => CFG_IBLOCK_STRINGS_ID,
        '=ID'				=> $id
];
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $string = $arFields;
    
}

if (empty($string)) return false;

$delta_x = $request->get('string_begin_position_x') - $string['PROPERTIES']['BEGIN_POSITION_X']['VALUE'];
$delta_y = $request->get('string_begin_position_y') - $string['PROPERTIES']['BEGIN_POSITION_Y']['VALUE'];


CIBlockElement::SetPropertyValueCode($id, "BEGIN_POSITION_X", $request->get('string_begin_position_x'));
CIBlockElement::SetPropertyValueCode($id, "BEGIN_POSITION_Y", $request->get('string_begin_position_y'));



$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "IBLOCK_SECTION_ID", "MODIFIED_BY", "ACTIVE", "PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         		=> CFG_IBLOCK_STORES_ID,
        'PROPERTY_STRING'			=> $string['ID']
];

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$stellage = [];

while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $stellage[] = $arFields;
}


$outPallets = [];

$movePallets = [];

foreach ($stellage as $key => $item)
{

	$_props = $item['PROPERTIES'];
	$PROP2 = [];
	foreach ($_props as $_prop)
	{
		$PROP2[$_prop['ID']] = $_prop['VALUE'];
	}

	$PROP = [];

	if ($PROP2[CFG_PROP_CELL1_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL1_COLUM_ID] > 0)
	{
		$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL1_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL1_ROW_ID], 'ID' => $item ['ID']];
		$PROP[CFG_PROP_CELL1_ROW_ID] 	= $PROP2[CFG_PROP_CELL1_ROW_ID] + $delta_y;
		$PROP[CFG_PROP_CELL1_COLUM_ID]	= $PROP2[CFG_PROP_CELL1_COLUM_ID] + $delta_x;
	}
	
	if ($PROP2[CFG_PROP_CELL2_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL2_COLUM_ID] > 0)
	{
		$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL2_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL2_ROW_ID], 'ID' => $item ['ID']];
		$PROP[CFG_PROP_CELL2_ROW_ID] 	= $PROP2[CFG_PROP_CELL2_ROW_ID] + $delta_y;
		$PROP[CFG_PROP_CELL2_COLUM_ID]	= $PROP2[CFG_PROP_CELL2_COLUM_ID] + $delta_x;
	}
	if ($PROP2[CFG_PROP_CELL3_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL3_COLUM_ID] > 0)
	{
		$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL3_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL3_ROW_ID], 'ID' => $item ['ID']];
		$PROP[CFG_PROP_CELL3_ROW_ID] 	= $PROP2[CFG_PROP_CELL3_ROW_ID] + $delta_y;
		$PROP[CFG_PROP_CELL3_COLUM_ID] 	= $PROP2[CFG_PROP_CELL3_COLUM_ID] + $delta_x;
	}
	if ($PROP2[CFG_PROP_CELL4_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL4_COLUM_ID] > 0)
	{
		$outPallets [] = ['X' => $PROP2[CFG_PROP_CELL4_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL4_ROW_ID], 'ID' => $item ['ID']];
		$PROP[CFG_PROP_CELL4_ROW_ID] 	= $PROP2[CFG_PROP_CELL4_ROW_ID] + $delta_y;
		$PROP[CFG_PROP_CELL4_COLUM_ID] 	= $PROP2[CFG_PROP_CELL4_COLUM_ID] + $delta_x;			
	}



	
	
	
	
	
	

	if ($PROP2[CFG_PROP_CELL1_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL1_COLUM_ID] > 0)
	{
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL1_ROW", $PROP[CFG_PROP_CELL1_ROW_ID]);
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL1_COLUM", $PROP[CFG_PROP_CELL1_COLUM_ID]);
	}

	if ($PROP2[CFG_PROP_CELL2_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL2_COLUM_ID] > 0)
	{
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL2_ROW", $PROP[CFG_PROP_CELL2_ROW_ID]);
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL2_COLUM", $PROP[CFG_PROP_CELL2_COLUM_ID]);
	}
	if ($PROP2[CFG_PROP_CELL3_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL3_COLUM_ID] > 0)
	{
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL3_ROW", $PROP[CFG_PROP_CELL3_ROW_ID]);
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL3_COLUM", $PROP[CFG_PROP_CELL3_COLUM_ID]);
	}
	if ($PROP2[CFG_PROP_CELL4_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL4_COLUM_ID] > 0)
	{
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL4_ROW", $PROP[CFG_PROP_CELL4_ROW_ID]);
		CIBlockElement::SetPropertyValueCode($item['ID'], "CELL4_COLUM", $PROP[CFG_PROP_CELL4_COLUM_ID]);
	}

	if ($PROP[CFG_PROP_CELL1_ROW_ID] > 0 && $PROP[CFG_PROP_CELL1_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL1_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL1_ROW_ID], 'ID' => $item ['ID']];
	}
	
	if ($PROP[CFG_PROP_CELL2_ROW_ID] > 0 && $PROP[CFG_PROP_CELL2_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL2_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL2_ROW_ID], 'ID' => $item ['ID']];
	}
	if ($PROP[CFG_PROP_CELL3_ROW_ID] > 0 && $PROP[CFG_PROP_CELL3_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL3_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL3_ROW_ID], 'ID' => $item ['ID']];
	}
	if ($PROP[CFG_PROP_CELL4_ROW_ID] > 0 && $PROP[CFG_PROP_CELL4_COLUM_ID] > 0)
	{
		$movePallets [] = ['X' => $PROP[CFG_PROP_CELL4_COLUM_ID], 'Y' => $PROP[CFG_PROP_CELL4_ROW_ID], 'ID' => $item ['ID']];
	}
//pr ($PROP);
}




$ReturnData = array(
	'SUCCESS'			=> 1,
	'OUT_PALLETS'	=> $outPallets,
	'MOVE_PALLETS'	=> $movePallets

);

//pr ($ReturnData);
//exit;

echo json_encode($ReturnData);
exit();

?>