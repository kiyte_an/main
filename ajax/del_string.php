<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if (intval ($request->get('ID')) == 0) return;

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');



$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         => CFG_IBLOCK_STRINGS_ID,
        'ID'				=> $request->get('ID'), 
        "ACTIVE_DATE"       => "Y", 
        "ACTIVE"            => "Y"];

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
$items = [];
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $items[] = $arFields;
}

$arResult['STRINGS'] = $items;

$arStringsID = [];

foreach ($arResult['STRINGS'] as $item)
{
    $arStringsID [] =  $item['ID'];
}

$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'             => CFG_IBLOCK_STORES_ID,
        'IBLOCK_SECTION_ID'     => $arParams['ID'],
        '=PROPERTY_STRING' => $arStringsID, 
        "ACTIVE_DATE"           => "Y", 
        "ACTIVE"                => "Y"];

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
$items = [];
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $items[] = $arFields;
    
}


CIBlockElement::Delete($request->get('ID'));


$shelvinges = $items;


$pallets = [];

$outPallets = [];


foreach ($shelvinges as $item)
{

	CIBlockElement::Delete($item['ID']);

	$stelage ['ID'] = $item['ID'];

    $colum1 = intval ($item['PROPERTIES']['CELL1_COLUM']['VALUE']);
    $row1 = intval ($item['PROPERTIES']['CELL1_ROW']['VALUE']);

    $colum2 = intval ($item['PROPERTIES']['CELL2_COLUM']['VALUE']);
    $row2 = intval ($item['PROPERTIES']['CELL2_ROW']['VALUE']);

    $colum3 = intval ($item['PROPERTIES']['CELL3_COLUM']['VALUE']);
    $row3 = intval ($item['PROPERTIES']['CELL3_ROW']['VALUE']);

    $colum4 = intval ($item['PROPERTIES']['CELL4_COLUM']['VALUE']);
    $row4 = intval ($item['PROPERTIES']['CELL4_ROW']['VALUE']);

    if ($colum1 > 0 && $row1 > 0)
    {
        $pallets [$row1][$colum1] = ['ID' => $item['ID']];
    }
    if ($colum2 > 0 && $row2 > 0)
    {
        $pallets [$row2][$colum2] = ['ID' => $item['ID']];
    }
    if ($colum3 > 0 && $row3 > 0)
    {
        $pallets [$row3][$colum3] = ['ID' => $item['ID']];
    }
    if ($colum4 > 0 && $row4 > 0)
    {
        $pallets [$row4][$colum4] = ['ID' => $item['ID']];
    }


	$PROP2 = [];
	foreach ($item ['PROPERTIES'] as $_prop)
	{
		
		$PROP2 [$_prop['ID']] = $_prop['VALUE'];
	}

  

	if ($PROP2[CFG_PROP_CELL1_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL1_COLUM_ID] > 0)
	{
	  $outPallets [] = ['X' => $PROP2[CFG_PROP_CELL1_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL1_ROW_ID], 'ID' => $stelage ['ID']];
	}
	
	if ($PROP2[CFG_PROP_CELL2_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL2_COLUM_ID] > 0)
	{
	  $outPallets [] = ['X' => $PROP2[CFG_PROP_CELL2_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL2_ROW_ID], 'ID' => $stelage ['ID']];
	}
	if ($PROP2[CFG_PROP_CELL3_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL3_COLUM_ID] > 0)
	{
	  $outPallets [] = ['X' => $PROP2[CFG_PROP_CELL3_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL3_ROW_ID], 'ID' => $stelage ['ID']];
	}
	if ($PROP2[CFG_PROP_CELL4_ROW_ID] > 0 && $PROP2[CFG_PROP_CELL4_COLUM_ID] > 0)
	{
	  $outPallets [] = ['X' => $PROP2[CFG_PROP_CELL4_COLUM_ID], 'Y' => $PROP2[CFG_PROP_CELL4_ROW_ID], 'ID' => $stelage ['ID']];
	}


}

$arResult ['PALLETS'] = $pallets;


$ReturnData = array(
	'SUCCESS'		=> 1,
	'OUT_PALLETS'	=> $outPallets
);

echo json_encode($ReturnData);
exit();

?>