<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();


if (intval ($request->get('ID') == 0) && empty ($request->get('NAME'))) return;



CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

$ID = $request->get('ID');
$zoneName = $request->get('NAME');


$bs = new CIBlockSection;
$arFields = [
	"NAME" 				=> $zoneName,
];

$res = $bs->update($ID, $arFields);


$ReturnData = array(
	'SUCCESS'	=> 1
);

echo json_encode($ReturnData);
exit();

?>