<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (empty ($_POST['NAME']) || intval ($_POST['WIDTH']) == 0 || intval ($_POST['HEIGHT']) == 0) return;
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

if ($_POST['UPDATE'] == 1)
{
	$ID = $_POST['STORE_ID'];
	$bs = new CIBlockSection;
	$arFields = [
		"NAME" 			=> $_POST['NAME'],
		"UF_COLUMN_COL"	=> $_POST['WIDTH'],
		"UF_ROW_COL" 	=> $_POST['HEIGHT']
	];
	$res = $bs->Update($ID, $arFields);
	$ReturnData = array(
		'SUCCESS'	=> 1,
		'ID'		=> $ID
	);

} else
{

	$bs = new CIBlockSection;
	$arFields = [
		"ACTIVE" 		=> "Y",
		"IBLOCK_ID" 	=> CFG_IBLOCK_STORES_ID,
		"NAME" 			=> $_POST['NAME'],
		"SORT" 			=> 500,
		"UF_COLUMN_COL"	=> $_POST['WIDTH'],
		"UF_ROW_COL" 	=> $_POST['HEIGHT']
	];
	$ID = $bs->Add($arFields);

	if ($ID > 0)
	{

		$bs = new CIBlockSection;
		$arFields = [
			"ACTIVE" 			=> "Y",
			"IBLOCK_ID" 		=> CFG_IBLOCK_STORES_ID,
			"NAME" 				=> CFG_ZONE1_NAME,
			"SORT"	 			=> 500,
			"IBLOCK_SECTION_ID" => $ID

		];

		$ID_Zone = $bs->Add($arFields);


		$ReturnData = array(
			'SUCCESS'	=> 1,
			'ID'		=> $ID
		);

	}
	else 
	{
		$ReturnData = array(
			'ERROR'		=> 1
		);
	}
	}
		echo json_encode($ReturnData);
		exit();
?>