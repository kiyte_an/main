<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (intval ($_POST['SECTION_ID']) == 0 || intval ($_POST['DAMAGE_ID']) == 0 || intval ($_POST['SHELF_ID'])==0) return;

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

//pr ($_POST);
//exit;

$arProps = helper::getAllProps (['IBLOCK_ID' => CFG_IBLOCK_DAMAGES_ID]);

$arPropsN = helper::getAllProps (['IBLOCK_ID' => CFG_IBLOCK_DAMAGE_REPORT_ID]);


foreach ($arProps as $key => &$_prop)
{
	$_propN = $arPropsN [$key];
	if ($_prop['PROPERTY_TYPE'] == 'L')
	{
		foreach ($_prop['VARIANTS'] as $key2 => &$_prop1)
		{
			$varId = 0;
			foreach ($_propN['VARIANTS'] as $keyN2 => $_propN1)
			{
				if ($_prop1['VALUE'] == $_propN1['VALUE'])
				{
					$varId = $_propN1['ID'];
				}
			}
			if ($varId > 0)
			{
				$_prop1['NEW_ID'] = $varId;
			}

		}
		unset ($_prop1);

	}
}
unset ($_prop);

$arPropsFull = $arProps;

/*
pr ($_POST);
pr ($arProps);
*/



//pr ($PROP);
//exit;

$dbItems = \Bitrix\Iblock\SectionTable::getList(array(
	'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
	'filter' => array('IBLOCK_ID' => CFG_IBLOCK_STORES_ID, 'ID' => $_POST['SECTION_ID'])
));


$store = $dbItems->fetch();

if (!$store) return;




$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         => CFG_IBLOCK_REPORTS_ID, 
        '=PROPERTY_STORE'   => $_POST['SECTION_ID'], 
        '=PROPERTY_STATUS'  => $GLOBALS ['STATUS']['open'], 
        "ACTIVE_DATE"       => "Y", 
        "ACTIVE"            => "Y"];
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $report = $arFields;
    
} 

if (!$report)
{
	$el = new CIBlockElement;

	$PROP = array();
	$PROP[CFG_PROP_STORE_ID] = $_POST['SECTION_ID'];  
	$PROP[CFG_PROP_STATUS_ID] = $GLOBALS ['STATUS']['open'];
	
	$arLoadProductArray = Array(
	  "MODIFIED_BY"    => $USER->GetID(), 
	  "IBLOCK_SECTION_ID" => false,
	  "IBLOCK_ID"      => CFG_IBLOCK_REPORTS_ID,
	  "PROPERTY_VALUES"=> $PROP,
	  "NAME"           => $store['NAME'],
	  "ACTIVE"         => "Y",
	  );
	
	$report ['ID'] = $el->Add($arLoadProductArray);
	
}


$el = new CIBlockElement;

$PROP = [];
$PROP[CFG_PROP_REPORT_ID] 	= $report ['ID'];
//$PROP[CFG_PROP_PRIORITY_ID] = $GLOBALS ['PRIORITET']['high'];
$PROP[CFG_PROP_DAMAGE_ID] = $_POST['DAMAGE_ID'];
$PROP[CFG_PROP_SHELVING_ID] = $_POST['SHELF_ID'];

/*
foreach ($_POST['PROP'] as $key => $_prop)
{
	if (!empty ($_prop))
	{
		$PROP [$key] = $_prop;
	}
}
*/



foreach ($_POST['FIELDS'] as $key => $_propF)
{
	if (!empty ($_propF['name']) && !empty ($_propF['value']))
	{
		$propID = intval (str_replace ('PROP_', '', $_propF['name']));
		if ($propID == 0) continue;

		foreach ($arPropsFull as $_prop)
		{
			if ($propID != $_prop['ID']) continue;

			if ($_prop['PROPERTY_TYPE'] == 'L')
			{
				foreach ($_prop['VARIANTS'] as $variant)
				{
					if ($variant['ID'] != $_propF['value']) continue;
					if ($variant['NEW_ID'] > 0)
					{
						$PROP [$_prop['CODE']] = $variant['NEW_ID'];
						break;
					}
				}

			} else
			{
				$PROP [$_prop['CODE']] = $_propF['value'];
			}
		}
		
	}
}





$arLoadProductArray = Array(
  "MODIFIED_BY"    => $USER->GetID(), 
  "IBLOCK_SECTION_ID" => false,
  "IBLOCK_ID"      => CFG_IBLOCK_DAMAGE_REPORT_ID,
  "PROPERTY_VALUES"=> $PROP,
  "NAME"           => 'Повреждение_' . time(),
  "ACTIVE"         => "Y",
  );


$report ['ID'] = $el->Add($arLoadProductArray);


//CFG_IBLOCK_DAMAGE_REPORT_ID

$class = $GLOBALS ['PRIORITET_CLASS'][$PROP['PRIORITY']];

//pr ($arLoadProductArray);
//exit;


$ReturnData = array(
	'SUCCESS'	=> 1,
	'CLASS'		=> $class
	);

	echo json_encode($ReturnData);
	exit();
?>