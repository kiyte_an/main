<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();


if (empty ($request->get('data')) || intval ($request->get('id')) == 0 || intval ($request->get('type'))==0) return;


$id = $request->get('id');
$data = $request->get('data');

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

$el = new CIBlockElement;


$data2 = [];
$dataN = [];

$arParamID = [];

foreach ($data as $key => $item)
{
	$x = $item['name'];
	$str = '$' . $x . ' = ' . $item['value'] . ';';

	//$prop[11885][55] = 70;
	eval ($str);

	if (!empty ($prop[0]) && is_array($prop[0]))
	{
		$dataN [] = $prop[0];
		unset ($prop[0]);
	}

	/*
	$ar = explode("_", $x);
	if ($ar[0] == 'prop' && is_numeric ($ar[1]) && is_numeric ($ar[2]))
	{
		$data2 [$ar[1]][$ar[2]] = $item;
	}
	*/
}


$data2 = $prop;
$arIndex = [];

foreach ($dataN as $k => $v)
{
	
	$s = array_flip($v);
	foreach ($s as $s2)
	{
		$arIndex [] = $s2;
	}
	
}
$arIndex = array_unique ($arIndex);
$dataN2 = [];
foreach ($arIndex as $key => $item)
{
	$i = 0;
	foreach ($dataN as $key2 => $item2)
	{
		foreach ($item2 as $key3 => $item3)
		{
			if ($item == $key3)
			{
				$dataN2 [$i][$item] = $item3;
				$i++;
			}
		}
	}
}


//pr ($data2);
//pr ($dataN2);

//exit;

foreach ($data2 as $key2 => $item2)
{
	$PROP = [];


	foreach ($item2 as $key => $item)
	{
		
		$PROP [intval ($key)]	= $item;

	}


//	CIBlockElement::SetPropertyValuesEx($key2, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
	$el->Update($key2, ["PROPERTY_VALUES" => $PROP]);

}


foreach ($dataN2 as $key2 => $item2) {
	$PROP = [];


	foreach ($item2 as $key => $item)
	{
		
		$PROP [intval ($key)]	= $item;

	}

	$arLoadProductArray2 = Array(
		"MODIFIED_BY"    => $USER->GetID(),
		"IBLOCK_ID"      => CFG_IBLOCK_ELEMENTS_ID,
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => 'Параметры_' . $id,
		"ACTIVE"         => "Y",
		);
	  
	  $paramID = $el->Add($arLoadProductArray2);
	  $arParamID[] = $paramID;
}

if (!empty ($arParamID))
{
	$arSelect   = ["ID", "NAME", "IBLOCK_ID"];
	$arFilter   = ["ID" => $id];
	$arOrder    = ["SORT" => "ASC", "ID" => "ASC"];
	$res = CIBlockElement::GetList($arOrder, $arFilter, false, Array(), $arSelect);
	$elem = [];
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();
		$arFields['PROPERTIES'] = $arProps;
		$elem = $arFields;
	}

	$arParamID2 = $arFields['PROPERTIES']['PARAMS']['VALUE'];

	$PROP = [];
	foreach ($arParamID2 as $key => $item)
	{
		$PROP[CFG_PROP_PARAMS_ID][] = $item;
	}

	foreach ($arParamID as $key => $item)
	{
		$PROP[CFG_PROP_PARAMS_ID][] = $item;
	}

	
	
	foreach ($PROP as $key => $item)
	{
		CIBlockElement::SetPropertyValuesEx($id, false, [$key => $item]);
	}
	//$el->Update($id, ["PROPERTY_VALUES" => $PROP]);
}

$ReturnData = array(
	'SUCCESS'	=> 1
);

echo json_encode($ReturnData);
exit();

?>