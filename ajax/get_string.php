<?
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();


if (intval ($request->get('id')) == 0) return;

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "IBLOCK_SECTION_ID", "MODIFIED_BY", "ACTIVE", "PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         => CFG_IBLOCK_STORES_ID,
        '=ID'				=> $request->get('id')
];
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $pallet = $arFields;
	$stringID = intval ($pallet['PROPERTIES']['STRING']['VALUE']);
}

$success = $stringID > 0 ? 1 : 0;

$ReturnData = array(
	'SUCCESS'			=> $success,
	'ID'				=> $stringID,
);

echo json_encode($ReturnData);
exit();

?>