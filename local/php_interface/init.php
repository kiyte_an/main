<?
include (__DIR__."/include/functions.php");
include (__DIR__."/include/class.helper.php");

//константы инфоблоков
//define('CFG_IBLOCK_REPORTS_ID', 		5);
define('CFG_IBLOCK_REPORTS_ID', 		7);
define('CFG_IBLOCK_STORES_ID',   		2);
define('CFG_IBLOCK_DAMAGES_ID',   		4);
define('CFG_IBLOCK_DAMAGE_REPORT_ID',		6);
define('CFG_IBLOCK_DAMAGE_REPORT2_ID',		7);
define('CFG_IBLOCK_STRINGS_ID',	        	8);
define('CFG_IBLOCK_REPORT_PARAMS_ID',   	9);
define('CFG_IBLOCK_ELEMENTS_ID',   	        10);


define('CFG_PROP_CELL1_ROW_ID',        		    11);
define('CFG_PROP_CELL1_COLUM_ID',      		    12);
define('CFG_PROP_CELL2_ROW_ID',        		    13);
define('CFG_PROP_CELL2_COLUM_ID',      		    14);
define('CFG_PROP_CELL3_ROW_ID',        		    15);
define('CFG_PROP_CELL3_COLUM_ID',      		    16);
define('CFG_PROP_CELL4_ROW_ID',        		    50);
define('CFG_PROP_CELL4_COLUM_ID',      		    51);

//define('CFG_PROP_STATUS_ID',        		24);
//define('CFG_PROP_STORE_ID',         		25);
define('CFG_PROP_STATUS_ID',			70);
define('CFG_PROP_STORE_ID',			71);

define('CFG_PROP_REPORT_ID',         		26);
define('CFG_PROP_PRIORITY_ID',         		27);
define('CFG_PROP_DAMAGE_ID',         		28);
define('CFG_PROP_SHELVING_ID',         		29);
define('CFG_PROP_DIRECTION_ID',        		45);
define('CFG_PROP_STRING_STORE_ID',        	46);
define('CFG_PROP_STRING_BEGIN_POSITION_X_ID',	47);
define('CFG_PROP_STRING_BEGIN_POSITION_Y_ID',   48);
define('CFG_PROP_NUM_SECTIONS_ID',              49);
define('CFG_PROP_STRING_ID',                    52);
define('CFG_PROP_PARAMS_ID',                    63);
define('CFG_PROP_ZONE_ID',                      130);
define('CFG_PROP_STRING_TYPE_ID',      		131);
define('CFG_PROP_NUM_PALLETS_ID',     		132);



define('CFG_ZONE1_NAME',                      'Зона 1');
define('CFG_ZONE_NAME',                       'Зона');


/*
$GLOBALS ['STATUS']    = [
    'open'  => 9,
    'close' => 10,
    ];
*/

$GLOBALS ['STATUS']    = [
    'open'  => 120,
    'close' => 121,
    ];

$GLOBALS ['PRIORITET']    = [
        'high'      => 11,
        'medium'    => 12,
        'low'       => 13,
        ];
    
$GLOBALS ['PRIORITET_CLASS'] = [
//    $GLOBALS ['PRIORITET']['high']      => '#bc382d',
//    $GLOBALS ['PRIORITET']['medium']    => '#f66901',
//    $GLOBALS ['PRIORITET']['low']       => '#f6f301'
    $GLOBALS ['PRIORITET']['high']      => 'prior_high',
    $GLOBALS ['PRIORITET']['medium']    => 'prior_medium',
    $GLOBALS ['PRIORITET']['low']       => 'prior_low'
];


//направление нитки
$GLOBALS ['DIRECTION']    = [
    'horizontal'    => 66,
    'vertical'      => 67,
    ];

//тип нитки
$GLOBALS ['TYPE']    = [
    'shelf'    => 252,
    'wall'     => 253,
    ];



//классы стеллажи
$GLOBALS ['CLASS_SECTION'] = [
	1 	=> ['vertical' => 'single','horizontal' => 'single-row'],
	2 	=> ['vertical' => 'dual','horizontal' => 'dual-row'],
	3 	=> ['vertical' => 'triple','horizontal' => 'triple-row'],
	4 	=> ['vertical' => 'quadruple','horizontal' => 'quadruple-row'],
	'end'	=> '-end'
];


function pr($x)
{
    echo '<pre>'; print_r ($x); echo '</pre>';
    return false;
}
