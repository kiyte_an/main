<?

class helper {
    //получаем списочные свойства по ID инфоблока и коду свойсва
    public function getProps ($params)
    {
        if (intval ($params['IBLOCK_ID']) == 0 || empty ($params['PROP_CODE'])) return false;

        $res = CIBlock::GetProperties($params['IBLOCK_ID'], [], ["CODE" => $params['PROP_CODE']]);
        $res_arr = $res->Fetch();

        if ($res_arr['PROPERTY_TYPE'] == 'S')
        {
            $arProps = [];
            $enum_fields ['PROPERTY_ID'] = $res_arr['ID'];
            $arProps[] = $enum_fields;
            return $arProps;

        }

        $property_enums = CIBlockPropertyEnum::GetList(["DEF"=>"DESC", "SORT"=>"ASC"], ["IBLOCK_ID" => $params['IBLOCK_ID'], "CODE" => $params['PROP_CODE']]);
        $arProps = [];
        while($enum_fields = $property_enums->GetNext())
        {
            $arProps[] = $enum_fields;
        }
        return $arProps;
    }

    //получаем все свойства с Инфоблока
    public function getAllProps ($params)
    {
        $iblock_id = intval($params['IBLOCK_ID']);
        if ($iblock_id == 0) return false;
        
        $properties = CIBlockProperty::GetList(["sort"=>"asc", "name"=>"asc"], ["ACTIVE"=>"Y", "IBLOCK_ID" => $iblock_id]);

        $arProps = [];
    
        while ($prop_fields = $properties->GetNext())
        {
            if ($prop_fields['PROPERTY_TYPE'] == 'L')
            {
                $property_enums = CIBlockPropertyEnum::GetList(["DEF"=>"DESC", "SORT"=>"ASC"], ["IBLOCK_ID" => $iblock_id, "CODE" => $prop_fields["CODE"]]);
                $variants = [];
                while($enum_fields = $property_enums->GetNext())
                {
                    $variants[] = $enum_fields;
                }
                $prop_fields['VARIANTS'] = $variants;
            }
    
            $arProps [$prop_fields['CODE']] = $prop_fields;
        }
        return $arProps;
    }    

    //реализация вывода свойств через select
    public function outPropsSelect ($p, $select = 0, $item = 0)
    {
        if ($item == 0)
        {
            $item = [];
            $item['ID'] = 0;
            //$item['ID'] = '';
        }

        $out = '';
        if (empty ($p))
        {
            $out = '';
            return $out;
        }

        //$out .= '<select name="prop_' . $item['ID'] . '_' . $p[0]['PROPERTY_ID'] . '">';
        $out .= '<select name="prop[' . $item['ID'] . '][' . $p[0]['PROPERTY_ID'] . ']">';
        $out .= '<option value="0">Выберите</option>';
        foreach ($p as $key => $_item)
        {
			$selectStr = '';
			if ($_item['ID'] == $select)
			{
				$selectStr = ' selected="selected"';
			}
            $out .= '<option value="' . $_item['ID'] . '"' . $selectStr . '>' . $_item['VALUE'] . '</option>';
        }
        $out .= '</select>';
        return $out;
    }


//реализация вывода свойств через input
public function outPropsInput ($p, $select = '', $item = 0)
{
    if ($item == 0)
    {
        $item = [];
        $item['ID'] = 0;
    }

    $out = '';
    $out .= '<input name="prop[' . $item['ID'] . '][' . $p[0]['PROPERTY_ID'] . ']" value="' . $select . '">';
    return $out;
}

    //выводим элементы иб по свойству
    public function getIblockElements ($p)
    {
        if (empty ($p)) return false;
        $arSelect   = ["ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID"];
        $arFilter   = ["ID" => $p];
        $arOrder    = ["SORT" => "ASC", "ID" => "ASC"];
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, Array(), $arSelect);
        $out = [];

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $arFields['PROPERTIES'] = $arProps;
            $out[] = $arFields;
        }
        return $out;
    }

    //выводим категории иб по свойству
    public function getIblockSections ($p)
    {
        if (empty ($p)) return false;
        $arSelect   = ["ID", "NAME", "IBLOCK_ID"];
        $arFilter   = ["ID" => $p];
        $arOrder    = ["SORT" => "ASC", "ID" => "ASC"];
        $res = CIBlockSection::GetList($arOrder, $arFilter,true,$arSelect);
        
        $out = [];

        while($arSect = $res->GetNext())
        {
            $out[] = $arSect;
        }
        return $out;
    }

    //выводим категории иб по ИнФоблоку
    public function getIblockSectionsIB ($arFilter)
    {
        if (empty ($arFilter)) return false;
        $arSelect   = ["ID", "NAME", "IBLOCK_ID"];
        $arOrder    = ["SORT" => "ASC", "NAME" => "ASC"];
        $res = CIBlockSection::GetList($arOrder, $arFilter,true,$arSelect);
        
        $out = [];

        while($arSect = $res->GetNext())
        {
            $out[] = $arSect;
        }
        return $out;
    }    

    //выводим массив повреждений по отчету
    public function getIblockDamages ($p)
    {
        if (empty ($p)) return false;
        $arSelect   = ["ID", "NAME", "IBLOCK_ID", "PROPERTY_REPORT"];
        $arFilter   = ["IBLOCK_ID" => CFG_IBLOCK_DAMAGE_REPORT_ID, "PROPERTY_REPORT.ID" => $p];
        $arOrder    = ["SORT" => "ASC", "ID" => "ASC"];
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, Array(), $arSelect);
        $out = [];

        $damages = [];
        $shelving = [];

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            $arFields['PROPERTIES'] = $arProps;
            $damages []     = $arFields['PROPERTIES']['DAMAGE']['VALUE'];
            $shelving []    = $arFields['PROPERTIES']['SHELVING']['VALUE'];

            $out[] = $arFields;
        }


        $arDamages  = self::getIblockElements ($damages);
        $arShelving = self::getIblockElements ($shelving);

        foreach ($out as $key => &$item)
        {
            foreach ($arDamages as $key2 => $item2)
            {
                if ($item['PROPERTIES']['DAMAGE']['VALUE'] == $item2['ID'])
                {
                    $item['PROPERTIES']['DAMAGE']['DAMAGE'] = $item2;
                    break;
                }
            }
        }
        unset ($item);

        foreach ($out as $key => &$item)
        {
            foreach ($arShelving as $key2 => $item2)
            {
                if ($item['PROPERTIES']['SHELVING']['VALUE'] == $item2['ID'])
                {
                    $item['PROPERTIES']['SHELVING']['SHELVING'] = $item2;
                    break;
                }
            }
        }
        unset ($item);


        //getIblockSections

        $sectionsID = [];

        foreach ($out as $key => $item)
        {
            $sectionsID[] = $item['PROPERTIES']['DAMAGE']['DAMAGE']['IBLOCK_SECTION_ID'];
        }

        $sectionsID = array_unique ($sectionsID);

        $sections = self::getIblockSections ($sectionsID);
        $_sections = [];
        foreach ($sections as $_sect)
        {
            $_sections [$_sect['ID']] = $_sect;
        }
        $sections = $_sections;
        
        foreach ($out as $key => &$item)
        {
            $sectID = $item['PROPERTIES']['DAMAGE']['DAMAGE']['IBLOCK_SECTION_ID'];
            $item['PROPERTIES']['DAMAGE']['DAMAGE']['SECTION'] = $sections [$sectID];
        }
//pr ($sections);



        return $out;
    }


    //выводим ид повреждений для склада
    public function getIblockDamagesID ($p)
    {
	$p = intval ($p);
        if ($p == 0) return false;
	
	CModule::IncludeModule('iblock');


        $arSelect   = ["ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID"];
        $arFilter   = ["PROPERTY_STORE" => $p, "IBLOCK_ID" => CFG_IBLOCK_REPORTS_ID];
        $arOrder    = ["SORT" => "ASC", "ID" => "ASC"];
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, Array(), $arSelect);
        $out = [];

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $out[] = $arFields['ID'];
        }

	if (empty ($out)) return false;

        $arSelect   = ["ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID"];
        $arFilter   = ["PROPERTY_REPORT" => $out, "IBLOCK_ID" => CFG_IBLOCK_DAMAGE_REPORT_ID];
        $arOrder    = ["SORT" => "ASC", "ID" => "ASC"];
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, Array(), $arSelect);
        $out2 = [];

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $out2[] = $arFields['ID'];
        }

	return $out2;
    }


    
}