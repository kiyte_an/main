<?

function test_store($storeID, $arFields)
{
//$arFields['NUM_SECTIONS'] = 12;
	$storeID = intval ($storeID);
	if ($storeID == 0) return;

	$arFilter = ['IBLOCK_ID' => CFG_IBLOCK_STORES_ID, 'ID' => $storeID];
	$arSelect = ['UF_ROW_COL', 'UF_COLUMN_COL'];
	$rsSections = CIBlockSection::GetList(['LEFT_MARGIN' => 'ASC'], $arFilter,false, $arSelect);
	$store = $rsSections->Fetch();

	$store_x = $store['UF_COLUMN_COL'];
	$store_y = $store['UF_ROW_COL'];

	if (!$store) return;

	$sections = [];
	for ($i = 0; $i < $arFields['NUM_SECTIONS']; $i++)
	{
		$x = $arFields['X'];
		$y = $arFields['Y'];
		if ($arFields['DIRECTION'] == $GLOBALS ['DIRECTION']['horizontal'])
		{
			$x = $x	 + $i;
		} else{
			$y = $y	 + $i;
		}
		$sections[] = ['X' => $x,'Y' => $y];
	}


	foreach ($sections as $_section)
	{
		if ($_section['X'] > $store_x || $_section['Y'] > $store_y)
		{
			$ReturnData = array(
				'SUCCESS'			=> 0,
				'ERROR'				=> 1,
				'MSG'				=> 'Ошибка добавления',
			);
			echo json_encode($ReturnData);
			exit();
		}
	}

	$sectionsPos = getSectionsPositionByStore ($storeID);



	foreach ($sectionsPos as $_section)
	{
		foreach ($sections as $_section2)	
		{
			if ($_section['X'] == $_section2['X'] && $_section['Y'] == $_section2['Y'])
			{

				$ReturnData = array(
					'SUCCESS'			=> 0,
					'ERROR'				=> 1,
					'MSG'				=> 'Ошибка добавления',
				);
				echo json_encode($ReturnData);
				exit();
			}
		}
	}

	return true;
}

function getSectionsPositionByStore ($storeID)
{
	$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "IBLOCK_SECTION_ID", "MODIFIED_BY", "ACTIVE", "PROPERTY_*"];
	$arFilter = [
			'IBLOCK_ID'         		=> CFG_IBLOCK_STORES_ID,
			'IBLOCK_SECTION_ID'			=> $storeID
	];
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	$stellage = [];
	
	$stellagePos = [];

	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();
		$arFields['PROPERTIES'] = $arProps;
		$stellage[] = $arFields;
		
		for ($i = 1; $i <=4; $i++)
		{
			if ($arProps['CELL' . $i . '_ROW']['VALUE'] > 0 && $arProps['CELL' . $i . '_COLUM']['VALUE'] > 0)
			{
				$stellagePos [] = ['X' => $arProps['CELL' . $i . '_COLUM']['VALUE'], 'Y' => $arProps['CELL' . $i . '_ROW']['VALUE'], 'ID' => $arFields['ID']];
			}
		}
	

	}
	return $stellagePos;

}