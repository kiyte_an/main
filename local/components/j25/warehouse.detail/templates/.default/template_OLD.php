<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<h1>Склад</h1>
<p>
	Для разметки склада воспользуйтесь мастером создания нитки, для редактирования склада - нажмите редактировать.
</p>
	<div class="navbar constructor-navbar">
		<button type="button" class="btn btn-success new-string-btn" data-toggle="modal" data-target="#exampleModal1">
			<i class="material-icons">auto_awesome_mosaic</i> Мастер создания нитки
		</button>
		<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal3">
			<i class="material-icons">settings</i> Редактировать склад
		</button>
	</div>
<?
$storeX = $arResult ['STORE']['UF_COLUMN_COL'];
$storeY = $arResult ['STORE']['UF_ROW_COL'];
?>
<div id="table-container">
	<table class="warehouse-layout">
		<tbody>
<?
for ($i = 0; $i < $storeY; $i++)
{
?>
			<tr>
<?
	for ($j = 0; $j < $storeX; $j++)
	{
?><td class="td-item" id="cell<?=$i?>_<?=$j?>" data-row="<?=$i?>" data-col="<?=$j?>" height="50">
<?
if (!empty ($arResult ['PALLETS'][$i][$j]))
{
?><span class="cell-item type1 item_num_<?=$arResult ['PALLETS'][$i][$j]['ID']?> triple" data-row="<?=$i?>" data-col="<?=$j?>" data-id="<?=$arResult ['PALLETS'][$i][$j]['ID']?>" data-first="1"></span>
<?
}
?>
</td>
<?
	}
?>
			</tr>
<?
}
?>
<?
/*
			<tr>
				<td class="td-item" id="cell0_0" data-row="0" data-col="0" height="158.3"><span
						class="cell-item type1 item_num_8256 triple" data-row="0" data-col="0" data-id="8256"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_1" data-row="0" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell0_2" data-row="0" data-col="2" height="158.3"><span
						class="cell-item type1 item_num_8257 triple" data-row="0" data-col="2" data-id="8257"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_3" data-row="0" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell0_4" data-row="0" data-col="4" height="158.3"><span
						class="cell-item type1 item_num_8258 dual" data-row="0" data-col="4" data-id="8258"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_5" data-row="0" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell0_6" data-row="0" data-col="6" height="158.3"><span
						class="cell-item type1 item_num_8259 triple-row" data-row="0" data-col="6" data-id="8259"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_7" data-row="0" data-col="7" height="158.3"><span
						class="cell-item type1 item_num_8259" data-row="0" data-col="7" data-id="8259" data-first="1"></span>
				</td>
				<td class="td-item" id="cell0_8" data-row="0" data-col="8" height="158.3"><span
						class="cell-item type1 item_num_8259 triple-row-and" data-row="0" data-col="8" data-id="8259"
						data-first="1"></span></td>
				<td class="td-item" id="cell0_9" data-row="0" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell1_0" data-row="1" data-col="0" height="158.3"><span
						class="cell-item type1 item_num_8256" data-row="1" data-col="0" data-id="8256" data-first="1"></span>
				</td>
				<td class="td-item" id="cell1_1" data-row="1" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell1_2" data-row="1" data-col="2" height="158.3"><span
						class="cell-item type1 item_num_8257" data-row="1" data-col="2" data-id="8257" data-first="1"></span>
				</td>
				<td class="td-item" id="cell1_3" data-row="1" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell1_4" data-row="1" data-col="4" height="158.3"><span
						class="cell-item type1 item_num_8258 dual-and" data-row="1" data-col="4" data-id="8258"
						data-first="1"></span></td>
				<td class="td-item" id="cell1_5" data-row="1" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell1_6" data-row="1" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell1_7" data-row="1" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell1_8" data-row="1" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell1_9" data-row="1" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell2_0" data-row="2" data-col="0" height="158.3"><span
						class="cell-item type1 item_num_8256 triple-and" data-row="2" data-col="0" data-id="8256"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_1" data-row="2" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell2_2" data-row="2" data-col="2" height="158.3"><span
						class="cell-item type1 item_num_8257 triple-and" data-row="2" data-col="2" data-id="8257"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_3" data-row="2" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell2_4" data-row="2" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell2_5" data-row="2" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell2_6" data-row="2" data-col="6" height="158.3"><span
						class="cell-item type1 item_num_8260 dual-row" data-row="2" data-col="6" data-id="8260"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_7" data-row="2" data-col="7" height="158.3"><span
						class="cell-item type1 item_num_8260 dual-row-and" data-row="2" data-col="7" data-id="8260"
						data-first="1"></span></td>
				<td class="td-item" id="cell2_8" data-row="2" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell2_9" data-row="2" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell3_0" data-row="3" data-col="0" height="158.3"><span class="cell-item type1 triple"></span></td>
				<td class="td-item" id="cell3_1" data-row="3" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell3_2" data-row="3" data-col="2" height="158.3"><span class="cell-item type1 dual"></span></td>
				<td class="td-item" id="cell3_3" data-row="3" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell3_4" data-row="3" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell3_5" data-row="3" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell3_6" data-row="3" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell3_7" data-row="3" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell3_8" data-row="3" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell3_9" data-row="3" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell4_0" data-row="4" data-col="0" height="158.3"><span class="cell-item type1"></span></td>
				<td class="td-item" id="cell4_1" data-row="4" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell4_2" data-row="4" data-col="2" height="158.3"><span class="cell-item type1 dual-and"></span></td>
				<td class="td-item" id="cell4_3" data-row="4" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell4_4" data-row="4" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell4_5" data-row="4" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell4_6" data-row="4" data-col="6" height="158.3"><span
						class="cell-item type1 item_num_8261 dual-row" data-row="4" data-col="6" data-id="8261"
						data-first="1"></span></td>
				<td class="td-item" id="cell4_7" data-row="4" data-col="7" height="158.3"><span
						class="cell-item type1 item_num_8261 dual-row-and" data-row="4" data-col="7" data-id="8261"
						data-first="1"></span></td>
				<td class="td-item" id="cell4_8" data-row="4" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell4_9" data-row="4" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell5_0" data-row="5" data-col="0" height="158.3"><span class="cell-item type1 triple-and"></span></td>
				<td class="td-item" id="cell5_1" data-row="5" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell5_2" data-row="5" data-col="2" height="158.3"><span class="cell-item type1 quadruple"></span></td>
				<td class="td-item" id="cell5_3" data-row="5" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell5_4" data-row="5" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell5_5" data-row="5" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell5_6" data-row="5" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell5_7" data-row="5" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell5_8" data-row="5" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell5_9" data-row="5" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell6_0" data-row="6" data-col="0" height="158.3"><span class="cell-item type1 dual"></span></td>
				<td class="td-item" id="cell6_1" data-row="6" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell6_2" data-row="6" data-col="2" height="158.3"><span class="cell-item type1"></span></td>
				<td class="td-item" id="cell6_3" data-row="6" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell6_4" data-row="6" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell6_5" data-row="6" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell6_6" data-row="6" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell6_7" data-row="6" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell6_8" data-row="6" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell6_9" data-row="6" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell7_0" data-row="7" data-col="0" height="158.3"><span class="cell-item type1 dual-and"></span></td>
				<td class="td-item" id="cell7_1" data-row="7" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell7_2" data-row="7" data-col="2" height="158.3"><span class="cell-item type1"></span></td>
				<td class="td-item" id="cell7_3" data-row="7" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell7_4" data-row="7" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell7_5" data-row="7" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell7_6" data-row="7" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell7_7" data-row="7" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell7_8" data-row="7" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell7_9" data-row="7" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell8_0" data-row="8" data-col="0" height="158.3"><span class="cell-item type1 dual"></span></td>
				<td class="td-item" id="cell8_1" data-row="8" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell8_2" data-row="8" data-col="2" height="158.3"><span class="cell-item type1 quadruple-end"></span></td>
				<td class="td-item" id="cell8_3" data-row="8" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell8_4" data-row="8" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell8_5" data-row="8" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell8_6" data-row="8" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell8_7" data-row="8" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell8_8" data-row="8" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell8_9" data-row="8" data-col="9" height="158.3"></td>
			</tr>
			<tr>
				<td class="td-item" id="cell9_0" data-row="9" data-col="0" height="158.3"><span class="cell-item type1 dual-and"></span></td>
				<td class="td-item" id="cell9_1" data-row="9" data-col="1" height="158.3"></td>
				<td class="td-item" id="cell9_2" data-row="9" data-col="2" height="158.3"><span class="cell-item type1 single"></span></td>
				<td class="td-item" id="cell9_3" data-row="9" data-col="3" height="158.3"></td>
				<td class="td-item" id="cell9_4" data-row="9" data-col="4" height="158.3"></td>
				<td class="td-item" id="cell9_5" data-row="9" data-col="5" height="158.3"></td>
				<td class="td-item" id="cell9_6" data-row="9" data-col="6" height="158.3"></td>
				<td class="td-item" id="cell9_7" data-row="9" data-col="7" height="158.3"></td>
				<td class="td-item" id="cell9_8" data-row="9" data-col="8" height="158.3"></td>
				<td class="td-item" id="cell9_9" data-row="9" data-col="9" height="158.3"></td>
			</tr>
			*/
?>
		</tbody>
	</table>
</div>

<div class="card table-responsive">
	<table class="table total-table">
		<thead>
			<tr>
				<td>Нитка</td>
				<td>Адрес (начальная точка)</td>
				<td>Количество секций</td>
				<?/*<td>Редактировать</td>*/?>
				<td>Копировать</td>
				<td>Переместить</td>
				<td>Удалить</td>
			<tr>
		</thead>
		<tbody id="string_tbody">
		<?
		foreach ($arResult['STRINGS'] as $key => $item)
		{
			?>
			<tr id="string_tr_<?=$item['ID']?>">
				<td><?=$item['NAME']?></td>
				<td id="string_position_<?=$item['ID']?>">(<?=$item['PROPERTIES']['BEGIN_POSITION_X']['VALUE']?>,<?=$item['PROPERTIES']['BEGIN_POSITION_Y']['VALUE']?>)</td>
				<td><?=$item['PROPERTIES']['NUM_SECTIONS']['VALUE']?></td>
				<?/*
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple edit-string" data-name="<?=$item['NAME']?>" data-toggle="modal" data-direction="<?=$item['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID']?>" data-target="#exampleModal1" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				*/
				?>
				<td>
					<button type="button" rel="tooltip"  class="btn btn-success btn-simple copy-string" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple move-string" data-toggle="modal" data-target="#exampleModalMove2" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple del-string" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<?
		}
		?>
		</tbody>
	</table>
</div>
<script>
var warehouse_id = '<?=intval ($arParams['ID'])?>';
var string_id = 0;
var pallete_x = 0;
var pallete_y = 0;
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="make-strings-form" data-warehouse-id="<?=$arParams['ID']?>">
		<div class="form-group">
            <label for="string-name" class="col-form-label">Название нитки:</label>
            <input type="text" class="form-control" id="string-name">
        </div>
		<div class="form-group">
            <label for="string-begin-position-x" class="col-form-label">Начальная позиция X:</label>
            <input type="number" class="form-control" id="string-begin-position-x">
        </div>
		<div class="form-group">
            <label for="string-begin-position-y" class="col-form-label">Начальная позиция Y:</label>
            <input type="number" class="form-control" id="string-begin-position-y">
        </div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">Направление</label>
			<select class="form-control" id="exampleFormControlSelect1">
			  <option value="<?=$GLOBALS ['DIRECTION']['horizontal']?>">Горизонтально</option>
			  <option value="<?=$GLOBALS ['DIRECTION']['vertical']?>">Вертикально</option>
			</select>
		  </div>
		  <div class="form-group">
            <label for="sections-num" class="col-form-label">Количество секций:</label>
            <input type="number" class="form-control" id="sections-num">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning make-strings-step2" data-toggle="modal" data-target="#exampleModal2">Далее</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="make-strings-step2-form">
        </form>
      </div>
      <div class="modal-footer make-strings-step2-footer">
        <button type="button" class="btn btn-success">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade new-warehouse-modal" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Редактирование склада</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="warehouse-name" class="col-form-label">Название:</label>
            <input type="text" class="form-control" id="warehouse-name" value="<?=$arResult['STORE']['NAME']?>">
          </div>
          <div class="form-group">
            <label for="warehouse-width" class="col-form-label">Ширина склада:</label>
            <input type="number" class="form-control" id="warehouse-width" value="<?=$arResult['STORE']['UF_COLUMN_COL']?>">
          </div>
		  <div class="form-group">
            <label for="warehouse-height" class="col-form-label">Высота склада:</label>
            <input type="number" class="form-control" id="warehouse-height"  value="<?=$arResult['STORE']['UF_ROW_COL']?>">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success update">Сохранить</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModalMove2" tabindex="-1" role="dialog" aria-labelledby="exampleModalMoveLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalMoveLabel">Мастер перемещения нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="move-strings-form">
		<div class="form-group">
            <label for="m-string-begin-position-x" class="col-form-label">Начальная позиция X:</label>
            <input type="number" class="form-control" id="m-string-begin-position-x">
        </div>
		<div class="form-group">
            <label for="m-string-begin-position-y" class="col-form-label">Начальная позиция Y:</label>
            <input type="number" class="form-control" id="m-string-begin-position-y">
        </div>
        </form>
      </div>
      <div class="modal-footer move-strings-footer">
        <button type="button" class="btn btn-success move">Сохранить</button>
      </div>
    </div>
  </div>
</div>