<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock(CFG_IBLOCK_STORES_ID);

$storeOb = $entity::getList(array(
		"select" => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'UF_ROW_COL', 'UF_COLUMN_COL'),
		"filter" => array('ID' => $arParams['ID'])
 ));

$store = $storeOb->fetch();
$arResult ['STORE'] = $store;


$arSelect = ["ID", "IBLOCK_ID","IBLOCK_SECTION_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
				'IBLOCK_ID'         => CFG_IBLOCK_STRINGS_ID,
				'=PROPERTY_STORE'   => $arParams['ID'], 
				"ACTIVE_DATE"       => "Y", 
				"ACTIVE"            => "Y"];

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
$items = [];
while($ob = $res->GetNextElement())
{
		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();
		$arFields['PROPERTIES'] = $arProps;
		$items[] = $arFields;
		
}

$arResult['STRINGS'] = $items;

$catID = [];
foreach ($arResult['STRINGS'] as $item)
{
	if ($item['PROPERTIES']["ZONE"]['VALUE'] > 0)
		$catID[] = $item['PROPERTIES']["ZONE"]['VALUE'];
}

$arCats = helper::getIblockSections ($catID);

//pr ($arCats);
//pr ($catID);

$arStringsID = [];

foreach ($arResult['STRINGS'] as $item)
{
		$arStringsID [] =  $item['ID'];
}

$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
				'IBLOCK_ID'             => CFG_IBLOCK_STORES_ID,
				'IBLOCK_SECTION_ID'     => $arParams['ID'],
				'=PROPERTY_STRING' => $arStringsID, 
				"ACTIVE_DATE"           => "Y", 
				"ACTIVE"                => "Y"];

//pr ($arFilter);

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
$items = [];
while($ob = $res->GetNextElement())
{
		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();
		$arFields['PROPERTIES'] = $arProps;
		if ($arFields['PROPERTIES']['STRING']['VALUE'] > 0)
		{
				foreach ($arResult['STRINGS'] as $_item)
				{
						if ($arFields['PROPERTIES']['STRING']['VALUE'] == $_item['ID'])
						{
								$arFields ['STRING'] = $_item;
						}
				}
		}


		if ($arFields['STRING']['PROPERTIES']['TYPE']['VALUE_ENUM_ID'] == $GLOBALS ['TYPE']['wall'])
		{

			$pallets_num = intval ($arFields['PROPERTIES']['NUM_SECTIONS']['VALUE']);

			$x_pos = intval ($arFields['STRING']['PROPERTIES']['BEGIN_POSITION_X']['VALUE']);
			$y_pos = intval($arFields['STRING']['PROPERTIES']['BEGIN_POSITION_Y']['VALUE']);

			if ($arFields['ID'] == 224792)
			{
	//			AddMessage2Log('pallets_num', $pallets_num);
	//			AddMessage2Log('x_pos', $x_pos);
	//			AddMessage2Log('y_pos', $y_pos);
		//    pr ($item);
			}


			for ($i = 0; $i < $pallets_num; $i++)
			{

				if ($arFields['STRING']['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID'] == $GLOBALS ['DIRECTION']['horizontal'])
				{
					$x_pos += 1;
				} else
				{
					$y_pos += 1;
				}
				$arFields['PROPERTIES']['CELL1_COLUM']['VALUE'] = $x_pos;
				$arFields['PROPERTIES']['CELL1_ROW']['VALUE'] 	= $y_pos;
				$items[] = $arFields;
			}
		//AddMessage2Log($arFields['STRING']['PROPERTIES']['TYPE'], 'xxx');
		} else
		{
			$items[] = $arFields;
		}
		
}



$shelvinges = $items;


$pallets = [];


foreach ($shelvinges as $item)
{
	if ($item['ID'] == 224786)
	{
		//AddMessage2Log('111', 'xxx');
//    pr ($item);
	}
		
		$strDirection = $item['STRING']['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID'];

		$colum1 = intval ($item['PROPERTIES']['CELL1_COLUM']['VALUE']);
		$row1 = intval ($item['PROPERTIES']['CELL1_ROW']['VALUE']);

		$colum2 = intval ($item['PROPERTIES']['CELL2_COLUM']['VALUE']);
		$row2 = intval ($item['PROPERTIES']['CELL2_ROW']['VALUE']);

		$colum3 = intval ($item['PROPERTIES']['CELL3_COLUM']['VALUE']);
		$row3 = intval ($item['PROPERTIES']['CELL3_ROW']['VALUE']);

		$colum4 = intval ($item['PROPERTIES']['CELL4_COLUM']['VALUE']);
		$row4 = intval ($item['PROPERTIES']['CELL4_ROW']['VALUE']);

		$numSections = 0;

		$isWall = ($item['STRING']['PROPERTIES']['TYPE']['VALUE_ENUM_ID'] == $GLOBALS ['TYPE']['wall']) ? 1 : 0;
//if ($item['ID'] == 224539)
//{


		for ($i = 1; $i <= 4; $i++)
		{
	$yName = 'CELL' . $i . '_ROW';
	$xName = 'CELL' . $i . '_COLUM';
	
	$_colum = $item["PROPERTIES"][$yName]["VALUE"];
	$_row = $item["PROPERTIES"][$xName]["VALUE"];

	if ($_colum >= 0 && $_row >= 0 && is_numeric ($_colum) && is_numeric ($_row))
	{
		$numSections++;
	}
		}

//pr ($numSections);
//}

if ($isWall == 1) {
	$addToClass = ' wallBlock';
} else {
	$addToClass = '';
}
		
		if ($colum1 >= 0 && $row1 >= 0 && is_numeric ($item['PROPERTIES']['CELL1_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL1_ROW']['VALUE']))
		{
				$class = '';
				$i = 1;
				$class = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? $GLOBALS ['CLASS_SECTION'][$i]['horizontal'] : $GLOBALS ['CLASS_SECTION'][$i]['vertical'];
				if ($numSections <= $i)
				{
						$class .= $GLOBALS ['CLASS_SECTION']['end'];
				}

				$class .= $addToClass;

				$pallets [$row1][$colum1] = ['ID' => $item['ID'], 'CLASS' => $class];
			 
		}

		if ($colum2 >= 0 && $row2 >= 0 && is_numeric ($item['PROPERTIES']['CELL2_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL2_ROW']['VALUE']))
		{
				$i = 2;
				$class = '';
				$class = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? $GLOBALS ['CLASS_SECTION'][$i]['horizontal'] : $GLOBALS ['CLASS_SECTION'][$i]['vertical'];

				if ($numSections <= $i)
				{
						$class .= $GLOBALS ['CLASS_SECTION']['end'];
				}

				$class .= $addToClass;

				$pallets [$row2][$colum2] = ['ID' => $item['ID'], 'CLASS' => $class];
		}
		if ($colum3 >= 0 && $row3 >= 0 && is_numeric ($item['PROPERTIES']['CELL3_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL3_ROW']['VALUE']))
		{
				$i = 3;
				$class = '';
				$class = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? $GLOBALS ['CLASS_SECTION'][$i]['horizontal'] : $GLOBALS ['CLASS_SECTION'][$i]['vertical'];
				if ($numSections <= $i)
				{
						$class .= $GLOBALS ['CLASS_SECTION']['end'];
				}

				$class .= $addToClass;

				$pallets [$row3][$colum3] = ['ID' => $item['ID'], 'CLASS' => $class];
		}
		if ($colum4 >= 0 && $row4 >= 0 && is_numeric ($item['PROPERTIES']['CELL4_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL4_ROW']['VALUE']))
		{
			$i = 4;
			$class = '';
			$class = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? $GLOBALS ['CLASS_SECTION'][$i]['horizontal'] : $GLOBALS ['CLASS_SECTION'][$i]['vertical'];

			if ($numSections <= $i)
			{
						$class .= $GLOBALS ['CLASS_SECTION']['end'];
			}
				

				$class .= $addToClass;

				$pallets [$row4][$colum4] = ['ID' => $item['ID'], 'CLASS' => $class];
		}
}

$arResult ['PALLETS'] = $pallets;



$arZones = helper::getIblockSectionsIB (['SECTION_ID' => $arResult ['STORE']['ID'], 'IBLOCK_ID' => CFG_IBLOCK_STORES_ID]);

if ($arResult ['STORE']['ID'])
{
		$arSelect   = ["ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID"];
		$arFilter   = ["PROPERTY_STORE" => $arResult ['STORE']['ID'], "IBLOCK_ID" => CFG_IBLOCK_REPORTS_ID];
		$arOrder    = ["SORT" => "DESC", "ID" => "DESC"];
		$res = CIBlockElement::GetList($arOrder, $arFilter, false, ["nPageSize"=>1], $arSelect);

		while($ob = $res->GetNextElement())
		{
				$arFields = $ob->GetFields();
				$arProps = $ob->GetProperties();
        $arFields['PROPERTIES'] = $arProps;
				$report = $arFields;
		}


	//pr ($arFilter);
	$arResult['PARAMS'] = helper::getIblockElements ($report['PROPERTIES']['PARAMS']['VALUE']);	
}


$params = [
	'IBLOCK_ID'     => CFG_IBLOCK_ELEMENTS_ID,
	'PROP_CODE'     => 'TYPE_OF_SHELF_EQUIPMENT'
];

$arResult['PROP_LIST1'] = helper::getProps ($params);

$params = [
'IBLOCK_ID'     => CFG_IBLOCK_ELEMENTS_ID,
'PROP_CODE'     => 'ELEMENT'
];

$arResult['PROP_LIST2'] = helper::getProps ($params);

$params = [
'IBLOCK_ID'     => CFG_IBLOCK_ELEMENTS_ID,
'PROP_CODE'     => 'BREND'
];

$arResult['PROP_LIST3'] = helper::getProps ($params);

$params = [
'IBLOCK_ID'     => CFG_IBLOCK_ELEMENTS_ID,
'PROP_CODE'     => 'SIZE'
];

$arResult['PROP_LIST4'] = helper::getProps ($params);

$params = [
'IBLOCK_ID'     => CFG_IBLOCK_ELEMENTS_ID,
'PROP_CODE'     => 'PREASURE'
];

$arResult['PROP_LIST5'] = helper::getProps ($params);


?>
<h1>Склад</h1>
<p>
Для разметки склада воспользуйтесь мастером создания нитки, для редактирования склада - нажмите редактировать.
</p>
	<div class="navbar constructor-navbar">
		<button type="button" class="btn btn-success new-string-btn" data-toggle="modal" data-target="#exampleModal1">
			<i class="material-icons">auto_awesome_mosaic</i> Создать
		</button>
		<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal3">
			<i class="material-icons">settings</i> Редактировать склад
		</button>
	</div>
<?
$storeX = $arResult ['STORE']['UF_COLUMN_COL'];
$storeY = $arResult ['STORE']['UF_ROW_COL'];
?>
<div id="table-section">
	<label for="scale">Масштаб: </label>
	<select name="scale" id="scale">
		<option value="50">50%</option>
		<option value="75">75%</option>
		<option value="100" selected>100%</option>
		<option value="150">150%</option>
		<option value="200">200%</option>
	</select>
	<div id="table-container">
		<table class="warehouse-layout">
			<tbody>
	<?
	for ($i = 0; $i < $storeY; $i++)
	{
	?>
				<tr>
	<?
		for ($j = 0; $j < $storeX; $j++)
		{
	?><td class="td-item" id="cell<?=$i?>_<?=$j?>" data-row="<?=$i?>" data-col="<?=$j?>" height="50">
	<?
	if (!empty ($arResult ['PALLETS'][$i][$j]))
	{
		$class = !empty ($arResult ['PALLETS'][$i][$j]['CLASS']) ? ' ' . $arResult ['PALLETS'][$i][$j]['CLASS'] : '';
	
	//  $GLOBALS ['TYPE']
	
	?><span class="cell-item type1 item_num_<?=$arResult ['PALLETS'][$i][$j]['ID']?><?=$class?>" data-row="<?=$i?>" data-col="<?=$j?>" data-id="<?=$arResult ['PALLETS'][$i][$j]['ID']?>" data-first="1"></span>
	<?
	}
	?>
	</td>
	<?
		}
	?>
				</tr>
	<?
	}
	?>
			</tbody>
		</table>
	</div>
</div>
<?
foreach ($arCats as &$_category)
{
	foreach ($arResult['STRINGS'] as $item)
	{
		if ($_category['ID'] == $item['PROPERTIES']["ZONE"]['VALUE'])
		{
			$_category['STRINGS'][] = $item;
		}
	}
}

unset ($_category);

//pr ($arCats);


?>
<div class="card table-responsive">
	<table class="table total-table">
		<thead>
			<tr>
				<td>Нитка</td>
				<td>Зона</td>
				<td>Адрес (начальная точка)</td>
				<td>Количество секций</td>
				<?/*<td>Редактировать</td>*/?>
				<td>Копировать</td>
				<td>Переместить</td>
				<td>Удалить</td>
			<tr>
		</thead>
		<tbody id="string_tbody">
		<?
		foreach ($arCats as $keyC => $itemC)
		{
			?>
			<tr class="zone-tr" id="string_zone_<?=$itemC['ID']?>">
				<td class="zone-td" colspan="7">
					<span id="string_zone_span_<?=$itemC['ID']?>"><?=$itemC['NAME']?></span>
					<button type="button" rel="tooltip" class="btn btn-success-green btn-simple zone-edit-modal" data-id="<?=$itemC['ID']?>" data-original-title="<?=$itemC['NAME']?>" title="<?=$itemC['NAME']?>">
							<i class="material-icons">edit</i>
						<div class="ripple-container"></div></button></td>
			</tr>
			<?
		foreach ($itemC['STRINGS'] as $key => $item)
		{
			?>
			<tr id="string_tr_<?=$item['ID']?>">
				<td><?=$item['NAME']?></td>
				<td id="string_zone_<?=$item['ID']?>">
						<select name="zones_<?=$item['ID']?>" class="selectpicker sel_zone" data-style="select-with-transition" title="Выберите зону" data-size="7"  id="string_zone_select_<?=$item['ID']?>">
						<option disabled>Выбрать зону</option>
						<?php
						foreach ($arZones as $_zone)
						{
							?>
							<option value="<?=$_zone['ID']?>"<?=($item['PROPERTIES']['ZONE']['VALUE'] == $_zone['ID'] ? ' selected="selected"': '')?>><?=$_zone['NAME']?></option>
						<?php
						}
						?>
						</select>
					</td>
				<td id="string_position_<?=$item['ID']?>">(<?=$item['PROPERTIES']['BEGIN_POSITION_X']['VALUE']?>,<?=$item['PROPERTIES']['BEGIN_POSITION_Y']['VALUE']?>)</td>
				<td><?=$item['PROPERTIES']['NUM_SECTIONS']['VALUE']?></td>
				<?/*
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple edit-string" data-name="<?=$item['NAME']?>" data-toggle="modal" data-direction="<?=$item['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID']?>" data-target="#exampleModal1" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				*/
				?>
				<td>
					<button type="button" rel="tooltip"  class="btn btn-success btn-simple copy-string" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple move-string" data-toggle="modal" data-target="#exampleModalMove2" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple del-string" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<?
		}
		}
		?>
		</tbody>
	</table>
</div>
<div>
		<button type="button" class="btn btn-zone-change">
			<i class="material-icons">auto_awesome_mosaic</i> Сохранить
			<div class="ripple-container"></div>
		</button>
		<button type="button" class="btn btn-zone-add" data-warehouse-id="<?=intval ($arParams['ID'])?>">
			<i class="material-icons">auto_awesome_mosaic</i> Создать зону
			<div class="ripple-container"></div>
		</button>
</div>
<script>
var warehouse_id = '<?=intval ($arParams['ID'])?>';
var string_id = 0;
var pallete_x = 0;
var pallete_y = 0;
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="make-strings-form" data-warehouse-id="<?=$arParams['ID']?>">
		<div class="form-group">
						<label for="string-name" class="col-form-label">Зона:</label>
						<input type="text" class="form-control" id="string-name">
				</div>
		<div class="form-group hidden">
						<label for="string-begin-position-x" class="col-form-label">Начальная позиция X:</label>
						<input type="number" class="form-control" id="string-begin-position-x">
				</div>
		<div class="form-group hidden">
						<label for="string-begin-position-y" class="col-form-label">Начальная позиция Y:</label>
						<input type="number" class="form-control" id="string-begin-position-y">
				</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">Направление</label>
			<select class="form-control" id="exampleFormControlSelect1">
				<option value="<?=$GLOBALS ['DIRECTION']['horizontal']?>">Горизонтально</option>
				<option value="<?=$GLOBALS ['DIRECTION']['vertical']?>">Вертикально</option>
			</select>
			</div>
		<div class="form-group">
			<label for="exampleFormControlType">Тип</label>
			<select class="form-control" id="exampleFormControlType">
				<option value="<?=$GLOBALS ['TYPE']['shelf']?>">Стеллаж</option>
				<option value="<?=$GLOBALS ['TYPE']['wall']?>">Стена</option>
			</select>
			</div>
					<div class="form-group sections-num-block">
						<label for="sections-num" class="col-form-label">Количество секций:</label>
						<input type="number" class="form-control" id="sections-num">
					</div>
					<div class="form-group d-none pallets-num-block">
						<label for="pallets-num" class="col-form-label">Количество палетомест:</label>
						<input type="number" class="form-control" id="pallets-num">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning make-strings-step2" data-toggle="modal" data-target="#exampleModal2">Далее</button>
				<button type="button" class="btn btn-warning make-wall d-none" data-toggle="modal" data-target="#exampleModal2">Сохранить</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="make-strings-step2-form">
				</form>
			</div>
			<div class="modal-footer make-strings-step2-footer">
				<button type="button" class="btn btn-success">Сохранить</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalFunctions" tabindex="-1" role="dialog" aria-labelledby="exampleModalFunctionsLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalFunctionsLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="modal-functions-block"></div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade new-warehouse-modal" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Редактирование склада</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="warehouse-name" class="col-form-label">Название:</label>
						<input type="text" class="form-control" id="warehouse-name" value="<?=$arResult['STORE']['NAME']?>">
					</div>
					<div class="form-group">
						<label for="warehouse-width" class="col-form-label">Ширина склада:</label>
						<input type="number" class="form-control" id="warehouse-width" value="<?=$arResult['STORE']['UF_COLUMN_COL']?>">
					</div>
			<div class="form-group">
						<label for="warehouse-height" class="col-form-label">Высота склада:</label>
						<input type="number" class="form-control" id="warehouse-height"  value="<?=$arResult['STORE']['UF_ROW_COL']?>">
					</div>
		<!--таблица из /templat/esempty/components/bitrix/news/reports/bitrix/news.detail/.default/template.php-->
			<table border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse;" id="report_table_4" class="table-modal" data-id="<?=$report['ID']?>">
			<tbody>
			<tr>
				<td style="background: #fbd4b4;">
					<p align="center">
						Тип стеллажного оборудования
					</p>
				</td>
				<td style="background: #fbd4b4;">
					<p align="center">
						Элемент
					</p>
				</td>
				<td style="background: #fbd4b4;">
					<p align="center">
						Размер
					</p>
				</td>
				<td style="background: #fbd4b4;">
					<p align="center">
						Нагрузка
					</p>
				</td>
				<td style="background: #fbd4b4;">
					<p align="center">
						Производитель
					</p>
				</td>
			</tr>
			<?
			//pr ($arResult['PARAMS']);
			foreach ($arResult['PARAMS'] as $key => $item)
			{
			?>
			<tr>
			<td>
	<?
	$select = $item['PROPERTIES']['TYPE_OF_SHELF_EQUIPMENT']['VALUE_ENUM_ID'];
	$out = helper::outPropsSelect ($arResult['PROP_LIST1'], $select, $item);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$select = $item['PROPERTIES']['ELEMENT']['VALUE_ENUM_ID'];
	$out = helper::outPropsSelect ($arResult['PROP_LIST2'], $select, $item);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$select = $item['PROPERTIES']['SIZE']['VALUE'];
	$out = helper::outPropsInput ($arResult['PROP_LIST4'],$select, $item);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$select = $item['PROPERTIES']['PREASURE']['VALUE'];
	$out = helper::outPropsInput ($arResult['PROP_LIST5'], $select, $item);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$select = $item['PROPERTIES']['BREND']['VALUE_ENUM_ID'];
	$out = helper::outPropsSelect ($arResult['PROP_LIST3'], $select, $item);
	?>
	<?=$out?>
	</td>
			</tr>
			<?
			}
			?>
			</tbody>
			</table>
			<script>
			var addToTable = `<tr>
			<td>
	<?
	$out = helper::outPropsSelect ($arResult['PROP_LIST1']);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$out = helper::outPropsSelect ($arResult['PROP_LIST2']);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$out = helper::outPropsInput ($arResult['PROP_LIST4']);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$out = helper::outPropsInput ($arResult['PROP_LIST5']);
	?>
	<?=$out?>
	</td>
	<td>
	<?
	$out = helper::outPropsSelect ($arResult['PROP_LIST3']);
	?>
	<?=$out?>
	</td>
			</tr>`;
			
			//report_table_4
			</script>
				
				<button type="button" class="btn btn-success_4" data-form-id="report_table_4">Сохранить </button>
				<button type="button" class="btn btn-add-plus-4" data-form-id="report_table_4">+</button>
			<!--/таблица из /templat/esempty/components/bitrix/news/reports/bitrix/news.detail/.default/template.php-->
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success update">Сохранить</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModalMove2" tabindex="-1" role="dialog" aria-labelledby="exampleModalMoveLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalMoveLabel">Мастер перемещения нитки</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="move-strings-form">
		<div class="form-group">
						<label for="m-string-begin-position-x" class="col-form-label">Начальная позиция X:</label>
						<input type="number" class="form-control" id="m-string-begin-position-x">
				</div>
		<div class="form-group">
						<label for="m-string-begin-position-y" class="col-form-label">Начальная позиция Y:</label>
						<input type="number" class="form-control" id="m-string-begin-position-y">
				</div>
				</form>
			</div>
			<div class="modal-footer move-strings-footer">
				<button type="button" class="btn btn-success move">Сохранить</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModalZoneEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalZoneEditLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalZoneEditLabel">Редактирование зоны</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="move-strings-form">
		<div class="form-group">
						<label for="zone-name" class="col-form-label">Название:</label>
						<input type="text" class="form-control" id="zone-name" value="">
				</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" data-modal="exampleModalZoneEdit" class="btn btn-success zone-save">Сохранить</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModalError1" tabindex="-1" role="dialog" aria-labelledby="exampleModalMoveLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalMoveLabel">Ошибка</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="error_msg"></div>
			</div>
		</div>
	</div>
</div>
