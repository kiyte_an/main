<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock(CFG_IBLOCK_STORES_ID);

$storeOb = $entity::getList(array(
    "select" => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'UF_ROW_COL', 'UF_COLUMN_COL'),
    "filter" => array('ID' => $arParams['ID'])
 ));

$store = $storeOb->fetch();
$arResult ['STORE'] = $store;


$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'         => CFG_IBLOCK_STRINGS_ID,
        '=PROPERTY_STORE'   => $arParams['ID'], 
        "ACTIVE_DATE"       => "Y", 
        "ACTIVE"            => "Y"];

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
$items = [];
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    $items[] = $arFields;
    
}

$arResult['STRINGS'] = $items;

$arStringsID = [];

foreach ($arResult['STRINGS'] as $item)
{
    $arStringsID [] =  $item['ID'];
}

$arSelect = ["ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*"];
$arFilter = [
        'IBLOCK_ID'             => CFG_IBLOCK_STORES_ID,
        'IBLOCK_SECTION_ID'     => $arParams['ID'],
        '=PROPERTY_STRING' => $arStringsID, 
        "ACTIVE_DATE"           => "Y", 
        "ACTIVE"                => "Y"];

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array(), $arSelect);
$items = [];
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();  
    $arProps = $ob->GetProperties();
    $arFields['PROPERTIES'] = $arProps;
    if ($arFields['PROPERTIES']['STRING']['VALUE'] > 0)
    {
        foreach ($arResult['STRINGS'] as $_item)
        {
            if ($arFields['PROPERTIES']['STRING']['VALUE'] == $_item['ID'])
            {
                $arFields ['STRING'] = $_item;
            }
        }
    }
    $items[] = $arFields;
    
}



$shelvinges = $items;


$pallets = [];

//pr ($shelvinges);

foreach ($shelvinges as $item)
{

    
    $strDirection = $item['STRING']['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID'];

    $colum1 = intval ($item['PROPERTIES']['CELL1_COLUM']['VALUE']);
    $row1 = intval ($item['PROPERTIES']['CELL1_ROW']['VALUE']);

    $colum2 = intval ($item['PROPERTIES']['CELL2_COLUM']['VALUE']);
    $row2 = intval ($item['PROPERTIES']['CELL2_ROW']['VALUE']);

    $colum3 = intval ($item['PROPERTIES']['CELL3_COLUM']['VALUE']);
    $row3 = intval ($item['PROPERTIES']['CELL3_ROW']['VALUE']);

    $colum4 = intval ($item['PROPERTIES']['CELL4_COLUM']['VALUE']);
    $row4 = intval ($item['PROPERTIES']['CELL4_ROW']['VALUE']);

    
    if ($colum1 >= 0 && $row1 >= 0 && is_numeric ($item['PROPERTIES']['CELL1_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL1_ROW']['VALUE']))
    {
        $class = '';
        $class = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? 'triple-row' : 'triple';
        if ($colum2 == 0 || $row2 == 0)
        {
            $class2 = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? 'triple-row-and' : 'triple-and';
            $class .= ' ' . $class2;
        }
        $pallets [$row1][$colum1] = ['ID' => $item['ID'], 'CLASS' => $class];
       
    }

    if ($colum2 >= 0 && $row2 >= 0 && is_numeric ($item['PROPERTIES']['CELL2_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL2_ROW']['VALUE']))
    {
        $class = '';
        if ($colum3 == 0 || $row3 == 0)
        {
            $class2 = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? 'triple-row-and' : 'triple-and';
            $class .= ' ' . $class2;
        }
        $pallets [$row2][$colum2] = ['ID' => $item['ID'], 'CLASS' => $class];
    }
    if ($colum3 >= 0 && $row3 >= 0 && is_numeric ($item['PROPERTIES']['CELL3_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL3_ROW']['VALUE']))
    {
        $class = '';
        if ($colum4 == 0 || $row4 == 0)
        {
            $class2 = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? 'triple-row-and' : 'triple-and';
            $class .= ' ' . $class2;
        }


        $pallets [$row3][$colum3] = ['ID' => $item['ID'], 'CLASS' => $class];
    }
    if ($colum4 >= 0 && $row4 >= 0 && is_numeric ($item['PROPERTIES']['CELL4_COLUM']['VALUE']) && is_numeric ($item['PROPERTIES']['CELL4_ROW']['VALUE']))
    {
            $class = '';
            $class2 = ($strDirection == $GLOBALS ['DIRECTION']['horizontal']) ? 'triple-row-and' : 'triple-and';
            $class .= ' ' . $class2;
        

        $pallets [$row4][$colum4] = ['ID' => $item['ID'], 'CLASS' => $class];
    }
}

$arResult ['PALLETS'] = $pallets;
//pr ($pallets);
