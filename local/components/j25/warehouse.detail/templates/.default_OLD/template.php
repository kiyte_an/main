<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<h1>Склад</h1>
<p>
Для разметки склада воспользуйтесь мастером создания нитки, для редактирования склада - нажмите редактировать.
</p>
	<div class="navbar constructor-navbar">
		<button type="button" class="btn btn-success new-string-btn" data-toggle="modal" data-target="#exampleModal1">
			<i class="material-icons">auto_awesome_mosaic</i> Мастер создания нитки
		</button>
		<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal3">
			<i class="material-icons">settings</i> Редактировать склад
		</button>
	</div>
<?
$storeX = $arResult ['STORE']['UF_COLUMN_COL'];
$storeY = $arResult ['STORE']['UF_ROW_COL'];
?>
<div id="table-container">
	<table class="warehouse-layout">
		<tbody>
<?
for ($i = 0; $i < $storeY; $i++)
{
?>
			<tr>
<?
	for ($j = 0; $j < $storeX; $j++)
	{
?><td class="td-item" id="cell<?=$i?>_<?=$j?>" data-row="<?=$i?>" data-col="<?=$j?>" height="50">
<?
if (!empty ($arResult ['PALLETS'][$i][$j]))
{
  $class = !empty ($arResult ['PALLETS'][$i][$j]['CLASS']) ? ' ' . $arResult ['PALLETS'][$i][$j]['CLASS'] : '';
?><span class="cell-item type1 item_num_<?=$arResult ['PALLETS'][$i][$j]['ID']?><?=$class?>" data-row="<?=$i?>" data-col="<?=$j?>" data-id="<?=$arResult ['PALLETS'][$i][$j]['ID']?>" data-first="1"></span>
<?
}
?>
</td>
<?
	}
?>
			</tr>
<?
}
?>
		</tbody>
	</table>
</div>

<div class="card table-responsive">
	<table class="table total-table">
		<thead>
			<tr>
				<td>Нитка</td>
				<td>Адрес (начальная точка)</td>
				<td>Количество секций</td>
				<?/*<td>Редактировать</td>*/?>
				<td>Копировать</td>
				<td>Переместить</td>
				<td>Удалить</td>
			<tr>
		</thead>
		<tbody id="string_tbody">
		<?
		foreach ($arResult['STRINGS'] as $key => $item)
		{
			?>
			<tr id="string_tr_<?=$item['ID']?>">
				<td><?=$item['NAME']?></td>
				<td id="string_position_<?=$item['ID']?>">(<?=$item['PROPERTIES']['BEGIN_POSITION_X']['VALUE']?>,<?=$item['PROPERTIES']['BEGIN_POSITION_Y']['VALUE']?>)</td>
				<td><?=$item['PROPERTIES']['NUM_SECTIONS']['VALUE']?></td>
				<?/*
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple edit-string" data-name="<?=$item['NAME']?>" data-toggle="modal" data-direction="<?=$item['PROPERTIES']['DIRECTION']['VALUE_ENUM_ID']?>" data-target="#exampleModal1" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">edit</i>
					</button>
				</td>
				*/
				?>
				<td>
					<button type="button" rel="tooltip"  class="btn btn-success btn-simple copy-string" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">content_copy</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-success btn-simple move-string" data-toggle="modal" data-target="#exampleModalMove2" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">open_with</i>
					</button>
				</td>
				<td>
					<button type="button" rel="tooltip" class="btn btn-danger btn-simple del-string" data-id="<?=$item['ID']?>" data-original-title="" title="">
						<i class="material-icons">close</i>
						<div class="ripple-container"></div>
					</button>
				</td>
			</tr>
			<?
		}
		?>
		</tbody>
	</table>
</div>
<script>
var warehouse_id = '<?=intval ($arParams['ID'])?>';
var string_id = 0;
var pallete_x = 0;
var pallete_y = 0;
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="make-strings-form" data-warehouse-id="<?=$arParams['ID']?>">
		<div class="form-group">
            <label for="string-name" class="col-form-label">Название нитки:</label>
            <input type="text" class="form-control" id="string-name">
        </div>
		<div class="form-group hidden">
            <label for="string-begin-position-x" class="col-form-label">Начальная позиция X:</label>
            <input type="number" class="form-control" id="string-begin-position-x">
        </div>
		<div class="form-group hidden">
            <label for="string-begin-position-y" class="col-form-label">Начальная позиция Y:</label>
            <input type="number" class="form-control" id="string-begin-position-y">
        </div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">Направление</label>
			<select class="form-control" id="exampleFormControlSelect1">
			  <option value="<?=$GLOBALS ['DIRECTION']['horizontal']?>">Горизонтально</option>
			  <option value="<?=$GLOBALS ['DIRECTION']['vertical']?>">Вертикально</option>
			</select>
		  </div>
		  <div class="form-group">
            <label for="sections-num" class="col-form-label">Количество секций:</label>
            <input type="number" class="form-control" id="sections-num">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning make-strings-step2" data-toggle="modal" data-target="#exampleModal2">Далее</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мастер создания нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="make-strings-step2-form">
        </form>
      </div>
      <div class="modal-footer make-strings-step2-footer">
        <button type="button" class="btn btn-success">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalFunctions" tabindex="-1" role="dialog" aria-labelledby="exampleModalFunctionsLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalFunctionsLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-functions-block"></div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade new-warehouse-modal" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Редактирование склада</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="warehouse-name" class="col-form-label">Название:</label>
            <input type="text" class="form-control" id="warehouse-name" value="<?=$arResult['STORE']['NAME']?>">
          </div>
          <div class="form-group">
            <label for="warehouse-width" class="col-form-label">Ширина склада:</label>
            <input type="number" class="form-control" id="warehouse-width" value="<?=$arResult['STORE']['UF_COLUMN_COL']?>">
          </div>
		  <div class="form-group">
            <label for="warehouse-height" class="col-form-label">Высота склада:</label>
            <input type="number" class="form-control" id="warehouse-height"  value="<?=$arResult['STORE']['UF_ROW_COL']?>">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success update">Сохранить</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModalMove2" tabindex="-1" role="dialog" aria-labelledby="exampleModalMoveLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalMoveLabel">Мастер перемещения нитки</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="move-strings-form">
		<div class="form-group">
            <label for="m-string-begin-position-x" class="col-form-label">Начальная позиция X:</label>
            <input type="number" class="form-control" id="m-string-begin-position-x">
        </div>
		<div class="form-group">
            <label for="m-string-begin-position-y" class="col-form-label">Начальная позиция Y:</label>
            <input type="number" class="form-control" id="m-string-begin-position-y">
        </div>
        </form>
      </div>
      <div class="modal-footer move-strings-footer">
        <button type="button" class="btn btn-success move">Сохранить</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModalError1" tabindex="-1" role="dialog" aria-labelledby="exampleModalMoveLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalMoveLabel">Ошибка</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="error_msg"></div>
      </div>
    </div>
  </div>
</div>