<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arResult = array();
\Bitrix\Main\Loader::includeModule('iblock');

if ($this->StartResultCache(false, false)) {

	$dbItems = \Bitrix\Iblock\SectionTable::getList(array(
		'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
		'filter' => array('IBLOCK_ID' => $arParams['IBLOCK_ID'])
	));

	$sections = [];


	while ($arItem = $dbItems->fetch())
	{
		$arItem ['IBLOCK_SECTION_ID'] = intval ($arItem ['IBLOCK_SECTION_ID']);
		$sections [] = $arItem;
	}


	$dbItems = \Bitrix\Iblock\ElementTable::getList(array(
		'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
		'filter' => array('IBLOCK_ID' => $arParams['IBLOCK_ID'])
	));

	$elements = [];
	while ($arItem = $dbItems->fetch()){
		// свойства элементов
		$dbProperty = \CIBlockElement::getProperty(
		$arItem['IBLOCK_ID'],
		$arItem['ID']
		); 
		while($arProperty = $dbProperty->Fetch())
		{
			$arItem['PROPERTIES'][] = $arProperty;
		}
		$elements [] = $arItem;
	}

	$arSections = [];

	foreach ($sections as &$item)
	{
		foreach ($elements as $item2)
		{
			if ($item['ID'] == $item2['IBLOCK_SECTION_ID'])
			{
				$item ['ELEMENTS'][] = $item2;
			}
		}

		if ($item['IBLOCK_SECTION_ID'] == 0)
		{
			$arSections[] = $item;
		}
	}
	unset ($item);
/*
	foreach ($arSections as &$item)
	{
		$item ['SUBSECTIONS'] = [];
		foreach ($sections as $item2)
		{
			if ($item['ID'] == $item2['IBLOCK_SECTION_ID'])
			{
				$item ['SUBSECTIONS'][] = $item2;
			}
		}


	}
	unset ($item);
*/

//	pr ($sections);
//	pr ($elements);
//  pr ($arSections);
	$arResult ['SECTIONS'] = $arSections;

    $this->IncludeComponentTemplate();
}

// Код, выполняющийся вне зависимости от кэша

?>
