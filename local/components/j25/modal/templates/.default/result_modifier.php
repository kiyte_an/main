<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$property_enums = CIBlockPropertyEnum::GetList(["SORT"=>"ASC", "NAME" => "ASC"], ["IBLOCK_ID"=>CFG_IBLOCK_DAMAGE_REPORT_ID, "CODE"=>"PRIORITY"]);
$arResult ['PRIORITY'] = [];
while($enum_fields = $property_enums->GetNext())
{
    $arResult ['PRIORITY'][] = $enum_fields;
}