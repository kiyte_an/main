<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => 'Вывод модальных окон',
	"DESCRIPTION" => 'Вывод модальных окон',
	"ICON" => "/images/news_detail.gif",
	//"ICON" => "/images/news_list.gif",
	"SORT" => 90,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "modal",
	),
);

?>
