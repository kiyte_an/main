<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arResult = array();
\Bitrix\Main\Loader::includeModule('iblock');

if ($this->StartResultCache(false, false)) {

	$arProps = helper::getAllProps (['IBLOCK_ID' =>CFG_IBLOCK_DAMAGES_ID]);


//	pr ($arProps);



	$dbItems = \Bitrix\Iblock\SectionTable::getList(array(
		'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
		'filter' => array('IBLOCK_ID' => $arParams['IBLOCK_ID'])
	));

	$sections = [];


	while ($arItem = $dbItems->fetch())
	{
		$arItem ['IBLOCK_SECTION_ID'] = intval ($arItem ['IBLOCK_SECTION_ID']);
		$sections [] = $arItem;
	}


	$dbItems = \Bitrix\Iblock\ElementTable::getList(array(
		'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
		'filter' => array('IBLOCK_ID' => $arParams['IBLOCK_ID'])
	));

	$elements = [];
	while ($arItem = $dbItems->fetch()){
		// свойства элементов
		$dbProperty = \CIBlockElement::getProperty(
		$arItem['IBLOCK_ID'],
		$arItem['ID']
		); 
		while($arProperty = $dbProperty->Fetch())
		{
			if ($arProperty['CODE'] == 'OUT_PROPERTIES')
			{
				$arItem['PROPERTIES'][$arProperty['CODE']][] = $arProperty;
			} else
			{
				$arItem['PROPERTIES'][$arProperty['CODE']] = $arProperty;				
			}

		}
		$elements [] = $arItem;
	}

	foreach ($elements as $key => &$item)
	{
		foreach ($item['PROPERTIES']['OUT_PROPERTIES'] as $item2)
		{
			$item['OUT_PROPERTIES'][] = $item2['VALUE_XML_ID'];
		}

		foreach ($item['OUT_PROPERTIES'] as $_prop)
		{
			$item['DISPLAY_PROPERTIES'][$_prop] = $item['PROPERTIES'][$_prop];
		}
	}

	unset ($item);

	//pr ($arParams['GET']['SECTION_ID']);


	$arSelect   = ["ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID"];
	$arFilter   = ["PROPERTY_STORE" => $arParams['GET']['SECTION_ID'], "IBLOCK_ID" => CFG_IBLOCK_REPORTS_ID];
	$arOrder    = ["SORT" => "DESC", "ID" => "DESC"];
	$arReports	= [];
	$arParamElements	= [];
	$res = CIBlockElement::GetList($arOrder, $arFilter, false, [], $arSelect);

	while($ob = $res->GetNextElement())
	{
			$arFields = $ob->GetFields();
			$arPropsReports = $ob->GetProperties();
			
			$arFields['PROPERTIES'] = $arPropsReports;
			$arReports[] = $arFields;
			foreach ($arFields['PROPERTIES']['PARAMS']['VALUE'] as $el)
			{
				$arParamElements[] = $el;
			}
			
	}
	
	$arParamElements = array_unique ($arParamElements);

	if (!empty ($arParamElements))
	{

		$dbItems = \Bitrix\Iblock\ElementTable::getList(array(
			'select' => array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID'),
			'filter' => array('IBLOCK_ID' => CFG_IBLOCK_ELEMENTS_ID, 'ID' => $arParamElements)
		));

		$__propsElements 	= [];
		$propsElements 		= [];
		while ($arItem = $dbItems->fetch()){
			// свойства элементов
			$dbProperty = \CIBlockElement::getProperty(
			$arItem['IBLOCK_ID'],
			$arItem['ID']
			); 
			while($arProperty = $dbProperty->Fetch())
			{
				if ($arProperty['CODE'] == 'ELEMENT' || $arProperty['CODE'] == 'SIZE')
				{
					$arItem ['PROPERTIES'][$arProperty['CODE']] = $arProperty;
					
				}
			}
			$__propsElements[] = $arItem;
		}

		foreach ($__propsElements as $key => $item)
		{
			$elName = $item['PROPERTIES']['ELEMENT']['VALUE_ENUM'];
			$propsElements [$elName][$item['PROPERTIES']['SIZE']['VALUE']] = $item['PROPERTIES']['SIZE']['VALUE'];
		}
	}

	$arResult['PROPS_ELEMENTS'] = $propsElements;
//	pr ($arResult['PROPS_ELEMENTS']);

/*
	foreach ($elements as $key => &$item)
	{
		if ($item['ID'] == 11537)
		{
			pr ($item['DISPLAY_PROPERTIES']);
		}
	}
*/

	$arSections = [];

	foreach ($sections as &$item)
	{
		foreach ($elements as $item2)
		{
			if ($item['ID'] == $item2['IBLOCK_SECTION_ID'])
			{
				$item ['ELEMENTS'][] = $item2;
			}
		}

		if ($item['IBLOCK_SECTION_ID'] == 0)
		{
			$arSections[] = $item;
		}
	}
	unset ($item);
/*
	foreach ($arSections as &$item)
	{
		$item ['SUBSECTIONS'] = [];
		foreach ($sections as $item2)
		{
			if ($item['ID'] == $item2['IBLOCK_SECTION_ID'])
			{
				$item ['SUBSECTIONS'][] = $item2;
			}
		}


	}
	unset ($item);
*/

//	pr ($sections);
//	pr ($elements);
//  pr ($arSections);
	$arResult ['SECTIONS'] 				= $arSections;
	$arResult ['IBLOCK_PROPERTIES'] 	= $arProps;

    $this->IncludeComponentTemplate();
}

// Код, выполняющийся вне зависимости от кэша

?>
